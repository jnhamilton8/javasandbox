package mokito.examples;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class DiscoveringMokito {

	@Test
	public void createSomeSimpleMockObjectsAndStubs() {

		//create the mock
		Calendar mockedCalendar = mock(Calendar.class);

		//define return value for the Calendar year
		when(mockedCalendar.get(Calendar.YEAR)).thenReturn(2020);

		assertEquals(mockedCalendar.get(Calendar.YEAR), 2020);

		//created my own

		CreateInteger testInt = mock(CreateInteger.class);
		when(testInt.get()).thenReturn(19);

		assertEquals(19, testInt.get());

		// example mocked list

		List myMockedList = mock(List.class);
		when(myMockedList.get(0)).thenReturn("candy");

		assertEquals(myMockedList.get(0),"candy");
	}

	@Test
	public void createStubThatReturnsMultipleValuesUsingSeveralChainedReturns() {
		Iterator i = mock(Iterator.class);
		when(i.next()).thenReturn("I love cake").thenReturn("it rocks");

		String result = i.next() + " " + i.next();
		assertEquals("The returned values from the iterator were incorrect", "I love cake it rocks", result);
	}

	@Test
	public void createStubThatReturnsMultipleValuesUsingOneReturnWithMultipleValues(){
		List myMockedList = mock(List.class);
		when(myMockedList.get(0)).thenReturn("pizza", "cake", "chocolate");

		assertEquals("pizza", myMockedList.get(0));
		assertEquals("cake", myMockedList.get(0));
		assertEquals("chocolate", myMockedList.get(0));
	}

	@Test
	public void createStubThatReturnsValuesBasedOnTheInput() {
		Comparable c = mock(Comparable.class);
		when(c.compareTo("Mockito")).thenReturn(1);
		when(c.compareTo("Intellij")).thenReturn(0);
		when(c.compareTo("cake")).thenReturn(-1);

		assertEquals(1, c.compareTo("Mockito"));
		assertEquals(0, c.compareTo("Intellij"));
		assertEquals(-1, c.compareTo("cake"));
	}

	@Test
	public void createStubThatReturnsValuesForAnyInputOfCCertainType() {
		Comparable x = mock(Comparable.class);
		when(x.compareTo(anyInt())).thenReturn(-1);

		assertEquals(-1, x.compareTo(9));

		List mockedArrayList = mock(ArrayList.class);
		when(mockedArrayList.get(anyInt())).thenReturn(5);
		when(mockedArrayList.isEmpty()).thenReturn(false);

		assertEquals(5, mockedArrayList.get(1));
		assertFalse(mockedArrayList.isEmpty());
	}

	@Test ()
	public void createStubToThrowExceptionUsingThenThrow(){
		List myMockedList = mock(ArrayList.class);
		doThrow(new RuntimeException()).when(myMockedList).clear();
		doThrow(new NullPointerException()).when(myMockedList).get(anyInt());

		assertThrows(NullPointerException.class, () -> myMockedList.get(anyInt()));
		assertThrows(RuntimeException.class, () -> myMockedList.clear());
	}

	@Test ()
	public void createStubToThrowExceptionUsingDoThrow(){
		List myMockedList2 = mock(ArrayList.class);
		doThrow(new RuntimeException()).when(myMockedList2).clear();
		assertThrows(RuntimeException.class, myMockedList2::clear);
	}

	@Test
	public void verifyStubbedMethodWasCalled(){
		List myMockedList3 = mock(List.class);

		myMockedList3.get(0);
		myMockedList3.clear();

		verify(myMockedList3).get(0);
		verify(myMockedList3).clear();
	}

	@Test
	public void verifyStubbedMethodWasCalledANumberOfTimes() {
		List myMockedList4 = mock(List.class);
		myMockedList4.clear();
		myMockedList4.get(0);
		myMockedList4.get(1);
		myMockedList4.add("a");
		myMockedList4.add("b");
		myMockedList4.add("c");
		verify(myMockedList4).clear();
		verify(myMockedList4, times(1)).clear();
		verify(myMockedList4, times(2)).get(anyInt());
		verify(myMockedList4, times(3)).add(anyObject());
		verify(myMockedList4, never()).remove(anyObject());
		verify(myMockedList4, atLeast(2)).add(anyObject());
		verify(myMockedList4, atMost(1)).clear();
	}
}