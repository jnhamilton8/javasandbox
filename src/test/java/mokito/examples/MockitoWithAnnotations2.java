package mokito.examples;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MockitoWithAnnotations2 {

	@Mock List<String> mockedList2; //set up Mock

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);  //initialise Mocks
	}

	@Test
	public void testUsingMockAnnotationsEnablingProgrammatically() {
		mockedList2.add("one");
		verify(mockedList2).add("one");
		assertEquals(0, mockedList2.size());

		when(mockedList2.size()).thenReturn(100);
		assertEquals(100, mockedList2.size());
	}
}


