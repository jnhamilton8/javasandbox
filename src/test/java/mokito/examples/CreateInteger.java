package mokito.examples;

public class CreateInteger {

	int n = 0;

	public CreateInteger(int n) {
		this.n = n;
	}

	public int get(){
		return n;
	}
}
