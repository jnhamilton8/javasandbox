package testNG.examples;

import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class TestNGExamples {

    //I am used in one test below to demo soft asserts
    SoftAssert softAssert = new SoftAssert();

    @BeforeClass
    public void setUp() {
        System.out.println("Some setup here");
    }

    @Test
    //it seems to need the public modifier to pick up the @Test annotation
    public void myFirstTestNGTest() {
        System.out.println("I have been run");
        //TestNG has it's own assertions - not sure how complete they are
        assertEquals(1, 1);
        //Looks like you can use Hamcrest too
        assertThat(1, equalTo(1));
    }

    @Test
    public void mySecondTestNGTest() {
        System.out.println("I have been run as well whoohoo");
        assertEquals("test", "test");
    }

    @Test(priority = 1) //lower priority is scheduled first
    public void myThirdTestNGTest() {
        System.out.println("I have a priority set!");
        assertEquals("choccy", "choccy");
    }

    @Test
    public void softAsserts() {
        //will carry on executing even on assert failure, rather than moving to the next @Test annotation
        System.out.println("I have soft asserts");

        softAssert.assertEquals("choccy", "nochoccy", "I got no choccy :(");
        softAssert.assertEquals("hi", "hi");
        softAssert.assertEquals("test", "test");

        softAssert.assertAll();
    }

    @Test(dependsOnMethods = "myFirstTestNGTest")
    public void dependsOnTest() {
        System.out.println("I depend on myFirstTestNGTest being run and I will run it before this one!");
        assertTrue("I am not true", true);
    }

    @Test(dependsOnGroups = "firstGroup")
    public void iDependOnAnotherGroup() {
        System.out.println("This method depends on the tests below, in the same group, running and passing");
    }

    @Test(groups = "firstGroup")
    public void iAmInAGroup() {
        System.out.println("Groups are a smart way to organise tests. Can be added to classes too");
    }

    @Test(groups = {"firstGroup", "secondGroup"})
    public void iAmInAGroupAsWell() {
        System.out.println("Groups are a smart way to organise tests. Can be added to classes too");
    }

    //Data Driven Testing

    //You can use DataProviders in other classes by using the dataProviderClass attribute (dataProviderClass = TestNGExamples.class)
    //in the @Test annotation, and make the @DataProvider method static.  So you can keep multiple DataProviders in the same place
    //and use them in as many places as you like
    @Test(dataProvider = "login-provider") //or can be the name of the @DataProvider method if it doesn't have a name
    public void login(String email, String password, boolean success) {

        System.out.println("Log in Credentials: " + "\n" +
                " Email = " + email + "\n" +
                " Password = " + password + "\n" +
                " Successful log in = " + success + "\n");
    }

    @DataProvider(name = "login-provider") //returns objects that are values to the login test above
    public Object[][] loginData() {  //provides data to the login method above
        Object[][] data = new Object[3][3];

        //3 rows of data, each passing values to email, password and success respectfully
        data[0][0] = "TestNG@Framework.com";
        data[0][1] = "TestNG1234";
        data[0][2] = true;
        data[1][0] = "Joe@Doe.com";
        data[1][1] = "DoeDoe34";
        data[1][2] = false;
        data[2][0] = "Test@AutomationU.com";
        data[2][1] = "TAU1234";
        data[2][2] = true;

        return data;
    }

    //Using parameters in the testng.xml file
    //These must be run via the xml file itself, not in the class.

    @Test
    @Parameters({"URL", "BrowserType"}) //get these from the xml file
    public void parameterTest(String url, String browserType) {
        System.out.println("This is the URL: " + url);

        System.out.printf("This is the browserType: %s", browserType);
    }

    @AfterClass
    public void cleanup() {
        System.out.println("I cleaned up some stuff!)");
    }
}
