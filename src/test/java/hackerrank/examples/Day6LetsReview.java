package hackerrank.examples;

import java.util.*;

public class Day6LetsReview {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner sc = new Scanner(System.in);
        int num_strings = sc.nextInt();

        String string[] = new String[num_strings];

        for (int i = 0; i < num_strings; i++) {
            string[i] = sc.next();
        }

        for (int temp = 0; temp < num_strings; temp++) {

            for (int j = 0; j < string[temp].length(); j = j + 2) {
                System.out.print(string[temp].charAt(j));
            }
            System.out.print(" ");

            for (int j = 1; j < string[temp].length(); j = j + 2) {
                System.out.print(string[temp].charAt(j));
            }

            System.out.println();
        }
        sc.close();
    }
}