package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BinarySearch {
    
    @Test
    void binarySearchFindsValue() {
        int[] sortedArray = {-22, -15, 1, 7, 30, 35, 55}; //must be sorted

        int result = BinarySearchImple.iterativeBinarySearch(sortedArray, -15);

        assertThat(result, equalTo(1));
    }

    public static class BinarySearchImple {

        public static int iterativeBinarySearch(int[] input, int value) {
            int start = 0;
            int end = input.length;

            while(start < end) { //when start == end we have searched entire array
                int midpoint = (start + end) / 2;
                if (input[midpoint] == value) {
                    return midpoint; //found what we are looking for
                }
                else if (input[midpoint] < value) { //search right part of the array
                    start = midpoint + 1;

                } else { // search left part of the array
                    end = midpoint;
                }
            }

            return -1; //did not find the value
        }
    }
}
