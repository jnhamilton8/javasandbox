package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SortingAlgorithms {

    @Test
    void bubbleSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = bubbleSort(unsortedArray);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void selectionSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = selectionSort(unsortedArray);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void insertionSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = insertionSort(unsortedArray);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void shellSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = shellSort(unsortedArray);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void mergeSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = mergeSort(unsortedArray, 0, unsortedArray.length);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void quickSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = quickSort(unsortedArray, 0, unsortedArray.length);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void countingSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {2, 5, 9, 8, 2, 8, 7, 10, 4, 3};
        int[] expectedSortedArray = {2, 2, 3, 4, 5, 7, 8, 8, 9, 10};

        int[] actualSortedArray = countingSort(unsortedArray, 1, unsortedArray.length);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void radixSortAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {4725, 4586, 1330, 8792, 1594, 5729};
        int[] expectedSortedArray = {1330, 1594, 4586, 4725, 5729, 8792};

        int[] actualSortedArray = radixSort(unsortedArray, 10, 4); //radix is the number of possible items e.g. 0-9, width = num of chars in each value

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void mergeSortAlgorithmSortsArrayHighestToLowest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {55, 35, 20, 7, 1, -15, -22};

        int[] actualSortedArray = mergeSortDescending(unsortedArray, 0, unsortedArray.length);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void insertionSortRecursiveAlgorithmSortsArrayLowestToHighest() {
        int[] unsortedArray = {20, 35, -15, 7, 55, 1, -22};
        int[] expectedSortedArray = {-22, -15, 1, 7, 20, 35, 55};

        int[] actualSortedArray = insertionSortRecursive(unsortedArray, unsortedArray.length);

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    @Test
    void radixSortAlgorithmSortsArrayOfStrings() {
        String[] unsortedArray = {"bcdef", "dbaqc", "abcde", "omadd", "bbbbb"};
        String[] expectedSortedArray = {"abcde", "bbbbb", "bcdef", "dbaqc", "omadd"};

        String[] actualSortedArray = radixSortString(unsortedArray, 26, 5); //radix is the number of possible items e.g. 26 chars in alphabet, width = length of each string

        assertThat(actualSortedArray, equalTo(expectedSortedArray));
    }

    /**
     * In place sorting algorithm that aims to reduce the large number of shifts in insertion sort or bubble sort.
     * Determines a gap value, sorts elements that are apart as far as gap interval, and reduces the interval (/2) until it becomes 1,
     * wherein it becomes insertion sort (but with a partially sorted array)
     * O(n2) worst but can perform much better (depends on the gap, and method used to choose the gap)
     *
     * @param anArray the array to sort using shell sort
     * @return the sorted array
     */
    public int[] shellSort(int[] anArray) {
        for (int gap = anArray.length / 2; gap > 0; gap /= 2) { //initialise gap value and reduce on each iteration

            for (int i = gap; i < anArray.length; i++) {
                int newElement = anArray[i];
                int j = i;

                while (j >= gap && anArray[j - gap] > newElement) {
                    anArray[j] = anArray[j - gap];
                    j -= gap;
                }

                anArray[j] = newElement;
            }
        }

        return anArray;
    }

    /**
     * In place sorting algorithm that partitions the array into sorted and unsorted positions but sorted partition is on the left
     * Assumes at first, the sorted partition is index 0
     * On each transversal takes the first unsorted element and inserts it onto the sorted partition, by transversing sorted partition
     * looking for a value that is <= the one we are trying to insert
     * Has O(n2) complexity.
     *
     * @param anArray the array to sort using insertion sort
     * @return the sorted array
     */
    private int[] insertionSort(int[] anArray) {
        for (int firstUnsortedIndex = 1; firstUnsortedIndex < anArray.length; firstUnsortedIndex++) { //growing sorted partition from L --> R so ++
            int newElement = anArray[firstUnsortedIndex];
            int i;

            for (i = firstUnsortedIndex; i > 0 && anArray[i - 1] > newElement; i--) { // keep going while we have not front of the array and the element before i in sorted partition is > the new element
                anArray[i] = anArray[i - 1]; // shifts element to right until i = 0 (front of array) or i - 1 is <= newElement
            }

            anArray[i] = newElement;  // insert the newElement into the correct position in the sorted partition

        }
        return anArray;
    }

    private int[] insertionSortRecursive(int[] input, int numItems) {
        if (numItems < 2) {
            return input;
        }

        insertionSortRecursive(input, numItems - 1); // by the time this returns first 6 items will have been sorted, we just need to sort the 7th item

        int newElement = input[numItems - 1];
        int i;

        for (i = numItems - 1; i > 0 && input[i - 1] > newElement; i--) { // keep going while we have not front of the array and the element before i in sorted partition is > the new element
            input[i] = input[i - 1]; // shifts element to right until i = 0 (front of array) or i - 1 is <= newElement
        }

        input[i] = newElement;  // insert the newElement into the correct position in the sorted partition

        return input;
    }

    /**
     * In place sorting algorithm that for each transversal selects the largest element and moves it to the sorted partition.
     * Transverses the array checking if i is > the index of the last recorded largest element and if so,
     * sets the index of the largest element to be i
     * After each iteration, swaps the largest element with the last item of the unsorted partition and increases the sorted
     * partition by 1
     * Starts with the unsorted index as the last item in the array.
     * Assumes the largest to start is the element at index 0
     * Has O(n2) complexity.
     *
     * @param anArray the array to sort using selection sort
     * @return the sorted array
     */
    private int[] selectionSort(int[] anArray) {
        for (int lastUnsortedIndex = anArray.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {
            int largest = 0;

            for (int i = 1; i <= lastUnsortedIndex; i++) { //need to compare the last element as well to see which is largest, hence <=
                if (anArray[i] > anArray[largest]) {
                    largest = i;
                }
            }

            swap(anArray, largest, lastUnsortedIndex); //swap the largest we found with the element at the last unsorted index
        }

        return anArray;
    }


    /**
     * In place sorting algorithm that keeps the sorted partition to the right.
     * Moves the highest value to the sorted partition at each iteration of the outer loop.
     * Each iteration of the outer loop increases the sorted partition by one.
     * Has O(n2) complexity.
     *
     * @param anArray the array to be sorted, lowest to highest
     * @return the sorted array
     */
    private int[] bubbleSort(int[] anArray) {
        for (int lastUnsortedIndex = anArray.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {
            for (int i = 0; i < lastUnsortedIndex; i++) { //comparing i against i + 1 so it will be <
                if (anArray[i] > anArray[i + 1]) {
                    swap(anArray, i, i + 1);
                }
            }
        }

        return anArray;
    }

    /**
     * Swaps the element at index i with the element at index j
     *
     * @param anArray array on ints on which to perform the swap operation
     * @param i       int which will be swapped with j
     * @param j       int which will be swapped with i
     */
    private void swap(int[] anArray, int i, int j) {
        if (i == j) {
            return;
        }

        int temp = anArray[i];
        anArray[i] = anArray[j];
        anArray[j] = temp;
    }

    /**
     * Uses divide and conquer.  In the splitting phase, the array is repeatedly divided into two until all the elements are separated individually
     * In the merging phase, pairs of elements are then compared, placed into order and combined.  The process is repeated until the array is recompiled as a whole
     * O(n log n) as repeatedly dividing the array in half during the splitting phase
     *
     * @param input the array to be sorted
     * @return the sorted array
     */
    private int[] mergeSort(int[] input, int start, int end) {
        if (end - start < 2) {
            return input;
        }

        // the split is done logically e.g. within same array
        int mid = (start + end) / 2;  //create the midpoint (any extra elements get put in right partition)

        mergeSort(input, start, mid); // for the left array
        mergeSort(input, mid, end); // for the right array

        merge(input, start, mid, end);

        return input;
    }

    private int[] mergeSortDescending(int[] input, int start, int end) {
        if (end - start < 2) {
            return input;
        }

        // the split is done logically e.g. within same array
        int mid = (start + end) / 2;  //create the midpoint (any extra elements get put in right partition)

        mergeSortDescending(input, start, mid); // for the left array
        mergeSortDescending(input, mid, end); // for the right array

        mergeInDescendingOrder(input, start, mid, end);

        return input;
    }

    private void merge(int[] input, int start, int mid, int end) {
        if (input[mid - 1] <= input[mid]) { //if all elements in left array < right we don't need to do anything
            return;
        }

        int i = start;
        int j = mid;
        int tempIndex = 0; //keep track of where are in temp array
        int[] temp = new int[end - start];
        while (i < mid && j < end) { //keep going until we have completed transversing left and right arrays
            temp[tempIndex++] = input[i] <= input[j] ? input[i++] : input[j++];
        }

        System.arraycopy(input, i, input, start + tempIndex, mid - i);
        System.arraycopy(temp, 0, input, start, tempIndex);
    }

    private void mergeInDescendingOrder(int[] input, int start, int mid, int end) {
        if (input[mid - 1] >= input[mid]) { //if all elements in left array < right we don't need to do anything
            return;
        }

        int i = start;
        int j = mid;
        int tempIndex = 0; //keep track of where are in temp array
        int[] temp = new int[end - start];
        while (i < mid && j < end) { //keep going until we have completed transversing left and right arrays
            temp[tempIndex++] = input[i] >= input[j] ? input[i++] : input[j++];
        }

        System.arraycopy(input, i, input, start + tempIndex, mid - i);
        System.arraycopy(temp, 0, input, start, tempIndex);
    }

    /**
     * Uses divide and conquer and is an in place algo, so uses less space than merge sort.
     * Picks an element as the pivot and partitions the array around the pivot, so items < pivot are to the left and those > pivot are to the right
     * Then recursively repeats quicksort on the left partition and right partition.
     * O(n log n) as repeatedly partitioning the array into two halves, worse case is quadratic. But usually better performer than merge sort.
     *
     * @param input the array to be sorted
     * @return the sorted array
     */
    private int[] quickSort(int[] input, int start, int end) {
        if (end - start < 2) { //one element array so don't have to do anything
            return input;
        }

        // returns the index of the pivot in the indexed array, in it's sorted position
        // everything smaller than pivot is to its left, and larger to its right
        int pivotIndex = partition(input, start, end);

        // now do the same with the left array and the right array
        quickSort(input, start, pivotIndex); // quick sort left sub array
        quickSort(input, pivotIndex + 1, end); // quick sort right sub array

        return input; // return sorted array!
    }

    private int partition(int[] input, int start, int end) {
        // This is using the first element as the pivot

        int pivot = input[start];
        int i = start;
        int j = end;

        while (i < j) { // if i > j it means they have crossed - i goes from left to right, and j from right to left
            // use j to look for elements that are < pivot
            // note use of --j -> this is so we decrement first then use the resulting value
            while (i < j && input[--j] >= pivot) ; // empty look body just used to decrement j

            if (i < j) { // if j hasn't crossed i, we have found the first element that is less than the pivot so move it to position i
                input[i] = input[j];
            }

            // use i to look for elements that are > pivot
            while (i < j && input[++i] <= pivot) ; // empty look body just used to increment i
            if (i < j) { // if j hasn't crossed i, we have found the first element that is > the pivot so move it to position j
                input[j] = input[i];
            }

        }

        input[j] = pivot; // set the pivot element back into the array

        return j; // return the index of the pivot element
    }

    /**
     * Iterates through the input counting the number of times each item occurs, then uses this to compute the item's index in the final sorted array
     * Not in place algo, makes the assumption the elements are discrete numbers and the range of items in the input is known ahead of time.
     * Works best when range of possible values is roughly equal to the number of elements you want to sort,
     * otherwise might have to create 1000 element array to sort 10 values!
     * O(n) - because we make assumptions about the data we are sorting
     *
     * @param input the array to be sorted
     * @return the sorted array
     */
    private int[] countingSort(int[] input, int min, int max) {
        int[] countArray = new int[(max - min) + 1];

        for (int i = 0; i < input.length; i++) {
            countArray[input[i] - min]++; //increment the position in countArray that corresponds to the value in i of input
        }

        //now done counting, need to write the values back into the input array
        int j = 0; // index used to write to the input array
        for (int i = min; i <= max; i++) {  // i is the index used to transverse the countArray
            while (countArray[i - min] > 0) { //each element in countArray has count which can be > 1
                input[j++] = i;
                countArray[i - min]--;
            }
        }

        return input; // return sorted array!
    }

    /**
     * Sorts data with integer values by grouping the values by individual digits that share the same significant position and value (place value).
     * i.e. sorts firstly those at place 1s, then 10s, then 100s etc. til the last place value where they are all sorted.
     * Radix sort uses counting sort as a subroutine to sort an array of numbers.
     * O(n) as we make assumptions above the data, but often O(n log n) because of the overhead involved
     * In place depends on which sorting algo is chosen
     *
     * @param input the array to be sorted
     * @return the sorted array
     */
    private int[] radixSort(int[] input, int radix, int width) {
        for (int i = 0; i < width; i++) {
            radixSingleSort(input, i, radix); //call this for each position in our values e.g. width of 4 = loop 4 times
        }

        return input; // return sorted array!
    }

    public void radixSingleSort(int[] input, int position, int radix) {
        int numItems = input.length;
        int[] countArray = new int[radix];

        for (int value : input) {
            countArray[getDigit(position, value, radix)]++; //counts the number of the values in the 1s, 10s, 100s etc positions
        }

        // Adjust the count array so it contains the number of values that have that digit or less than that digit in the position we are working with
        for (int j = 1; j < radix; j++) {
            countArray[j] += countArray[j - 1];
        }

        int[] temp = new int[numItems];

        // copy values into the temp array in sorted order
        for (int tempIndex = numItems - 1; tempIndex >= 0; tempIndex--) {
            temp[--countArray[getDigit(position, input[tempIndex], radix)]] = input[tempIndex];
        }

        // copy from temp array to input array
        System.arraycopy(temp, 0, input, 0, numItems);
    }

    private String[] radixSortString(String[] input, int radix, int width) {
        for (int i = width -1; i >=0; i--) {
            radixSingleSortString(input, i, radix); //call this for each position in our values e.g. width of 4 = loop 4 times
        }

        return input; // return sorted array!
    }

    public void radixSingleSortString(String[] input, int position, int radix) {
        int numItems = input.length;
        int[] countArray = new int[radix];

        for (String value : input) {
            countArray[getIndex(position, value)]++; //counts the number of the values in the 1s, 10s, 100s etc positions
        }

        // Adjust the count array so it contains the number of values that have that char or less than that char in the position we are working with
        for (int j = 1; j < radix; j++) {
            countArray[j] += countArray[j - 1];
        }

        String[] temp = new String[numItems];

        // copy values into the temp array in sorted order
        for (int tempIndex = numItems - 1; tempIndex >= 0; tempIndex--) {
            temp[--countArray[getIndex(position, input[tempIndex])]] = input[tempIndex];
        }

        // copy from temp array to input array
        System.arraycopy(temp, 0, input, 0, numItems);
    }

    private int getDigit(int position, int value, int radix) {
        return value / (int) Math.pow(10, position) % radix; //
    }

    private int getIndex(int position, String value) {
        return value.charAt(position) - 'a'; // gets the position in the array, as a = 096, so value is the string e.g 'bbbbb', position might be 4 which is 'b' = 098.  098 - 096 = 2
    }
}
