package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

public class HeapOperations {

    @Test
    void canUseTheHeap() {
        Heap heap = new Heap(10);

        heap.insert(80);
        heap.insert(75);
        heap.insert(60);
        heap.insert(68);
        heap.insert(55);
        heap.insert(40);
        heap.insert(52);
        heap.insert(67);

        heap.printHeap();

        //heap.delete(5);
        //heap.printHeap();

        //System.out.println(heap.peek());

        heap.sort();
        heap.printHeap();
    }

}
