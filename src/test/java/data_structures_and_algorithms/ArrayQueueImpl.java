package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ArrayQueueImpl {

    @Test
    void newArrayQueueOperations() {
        ArrayQueueImpl.ArrayQueue queue = new ArrayQueueImpl.ArrayQueue(10);
        queue.add(new Employee("Jane", "Jones", 123));
        queue.add(new Employee("John", "Doe", 4567));
        queue.add(new Employee("Mary", "Smith", 22));
        queue.add(new Employee("Mike", "Wilson", 3245));
        queue.add(new Employee("Bill", "End", 78));

        assertThat(queue.size(), equalTo(5));
        assertThat(queue.remove(), equalTo(new Employee("Jane", "Jones", 123)));
        assertThat(queue.peek(), equalTo(new Employee("John", "Doe", 4567)));

        queue.printQueue();
    }

    public static class ArrayQueue {

        private Employee[] queue;
        private int front;
        private int back;  // always at back -1, the next available position in the queue

        public ArrayQueue(int capacity) {
            queue = new Employee[capacity];
        }

        public void add(Employee employee) {
            if (size() == queue.length - 1) {
                int numItems = size();
                Employee[] newArray = new Employee[2 * queue.length];
                System.arraycopy(queue, front, newArray, 0, queue.length - front); //copy items from back to front
                System.arraycopy(queue, 0, newArray, queue.length - front, back); //copy items from front to behind the back items just moved to front
                queue = newArray;

                front = 0;  //reset front and back
                back = numItems;
            }

            queue[back] = employee;
            if (back < queue.length - 1) {
                back++;
            } else { // we have reached end of the array
                back = 0;  //wrap the back to the front to use any empty space at the front
            }
        }

        public Employee remove() {
            if (size() == 0) {
                throw new NoSuchElementException();
            }

            Employee employee = queue[front];
            queue[front] = null;
            front++;

            if (size() == 0) { // if nothing left in the queue reset it
                front = 0;
                back = 0;
            } else if (front == queue.length) { // if front index has reached end of the queue
                front = 0;
            }

            return employee;
        }

        public Employee peek() {
            if (size() == 0) {
                throw new NoSuchElementException();
            }

            return queue[front];
        }

        public int size() {
            if (front <= back) { // queue hasn't wrapped
                return back - front;
            } else {
                return back - front + queue.length;
            }

        }

        public void printQueue() {
            if (front <= back) { // queue hasn't wrapped
                for (int i = front; i < back; i++) {
                    System.out.println(queue[i]);  //prints queue from front to back
                }
            } else {
                for (int i = front; i < queue.length; i++) { // print the items from front pointer to end of array
                    System.out.println(queue[i]);
                }

                for (int i = 0; i < back; i++) { // print the items from beginning of array to back pointer
                    System.out.println(queue[i]);
                }
            }
        }
    }
}
