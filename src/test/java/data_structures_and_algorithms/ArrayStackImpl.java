package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ArrayStackImpl {

    @Test
    void newArrayStackOperations() {
        ArrayStack stack = new ArrayStack(10);
        stack.push(new Employee("Jane", "Jones", 123));
        stack.push(new Employee("John", "Doe", 4567));
        stack.push(new Employee("Mary", "Smith", 22));
        stack.push(new Employee("Mike", "Wilson", 3245));
        stack.push(new Employee("Bill", "End", 78));

        assertThat(stack.size(), equalTo(5));
        assertThat(stack.pop(), equalTo(new Employee("Bill", "End", 78)));
        assertThat(stack.peek(), equalTo(new Employee("Mike", "Wilson", 3245)));

        stack.printStack();
    }

    public static class ArrayStack {

        private Employee[] stack;
        private int top; //contains index of the next available position in the backing array

        public ArrayStack(int capacity) {
            stack = new Employee[capacity];
        }

        public void push(Employee employee) {
            if (top == stack.length) {
                // need to resize the backing array
                Employee[] newArray = new Employee[2 * stack.length];
                System.arraycopy(stack, 0, newArray, 0, stack.length);
                stack = newArray;
            }

            stack[top++] = employee; //assign employee to top, then increment top to be next available position
        }

        public Employee pop() {
            if (isEmpty()) {
                throw new EmptyStackException();
            }

            Employee employee = stack[--top]; //decrement top
            stack[top] = null;
            return employee;
        }

        public Employee peek() {
            if (isEmpty()) {
                throw new EmptyStackException();
            }

            return stack[top - 1]; // just return the top value which is the index of top -1
        }

        public int size() {
            return top;
        }

        public boolean isEmpty() {
            return top == 0;
        }

        public void printStack() {
            for (int i = top - 1; i >= 0; i--) {
                System.out.println(stack[i]);  //prints stack from top to bottom
            }
        }
    }
}
