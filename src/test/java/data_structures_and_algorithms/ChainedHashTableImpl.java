package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.ListIterator;

public class ChainedHashTableImpl {

    @Test
    void newChainedHashTableOperations() {
        ChainedHashtable hashTable = new ChainedHashtable();
        hashTable.put("Jones", new Employee("Jane", "Jones", 123));
        hashTable.put("Doe", new Employee("John", "Doe", 4567));
        hashTable.put("Smith", new Employee("Mary", "Smith", 22));
        hashTable.put("Wilson", new Employee("Mike", "Wilson", 3245));

        hashTable.printHashTable();

        System.out.println("Retrieve key Wilson: " + hashTable.get("Wilson"));
        System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));

        hashTable.remove("Wilson");
        hashTable.remove("Jones");

        hashTable.printHashTable();

        System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));
    }

    public static class ChainedHashtable {

        private LinkedList<StoredEmployee>[] hashTable;

        public ChainedHashtable() {
            hashTable = new LinkedList[10];
            for (int i = 0; i < hashTable.length; i++) {
                hashTable[i] = new LinkedList<>();
            }
        }

        public void put(String key, Employee employee) {
            int hashedKey = hashKey(key);
            hashTable[hashedKey].add(new StoredEmployee(key, employee));
        }

        public Employee get(String key) {
            int hashedKey = hashKey(key);
            ListIterator<StoredEmployee> iterator = hashTable[hashedKey].listIterator();
            StoredEmployee employee = null;

            while (iterator.hasNext()) {
                employee = iterator.next();
                if (employee.key.equals(key)) {
                    return employee.employee;
                }
            }

            return null; //we didn't find the employee
        }

        public Employee remove(String key) {
            int hashedKey = hashKey(key);
            ListIterator<StoredEmployee> iterator = hashTable[hashedKey].listIterator();
            StoredEmployee employee = null;
            int index = -1;

            while (iterator.hasNext()) {
                employee = iterator.next();
                index++;
                if (employee.key.equals(key)) {
                    break;
                }
            }

            if (employee == null) {
                return null; //we didn't find the employee
            } else {
                hashTable[hashedKey].remove(index);
                return employee.employee;
            }
        }

        private int hashKey(String key) {
            return Math.abs(key.hashCode() % hashTable.length); //using hashCode from String, modding by length of hashtable so will get keys from 0-9
        }

        public void printHashTable() {
            for (int i = 0; i < hashTable.length; i++) {
                if (hashTable[i].isEmpty()) {
                    System.out.println("Position " + i + ": empty");
                } else {
                    System.out.print("Position " + i + ": ");
                    ListIterator<StoredEmployee> iterator = hashTable[i].listIterator();
                    while (iterator.hasNext()) {
                        System.out.print(iterator.next().employee);
                        System.out.println(" -> ");
                    }
                    System.out.println("null");
                }
            }
        }
    }
}
