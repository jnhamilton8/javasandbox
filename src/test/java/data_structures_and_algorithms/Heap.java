package data_structures_and_algorithms;

public class Heap {

    private int[] heap;
    private int size;

    public Heap(int capacity) {
        heap = new int[capacity];
    }

    public int peek() {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException("Heap is empty!");
        }

        return heap[0]; //always return root
    }

    public void insert(int value) {
        if (isFull()) {
            throw new IndexOutOfBoundsException("Heap is full!");
        }

        heap[size] = value;

        fixHeapAbove(size);
        size++; //use current value of size then increment to next position
    }

    public int delete(int index) {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException("Heap is empty!");
        }

        int parent = getParent(index);
        int deletedValue = heap[index];

        heap[index] = heap[size - 1]; //size - 1 will yield the right most value

        if (index == 0 || heap[index] < heap[parent]) {
            fixHeapBelow(index, size - 1); // if we deleted the root, or the replacement value is < the parent, we need to look down the tree
        } else {
            fixHeapAbove(index);
        }
        size--;

        return deletedValue;
    }

    public void sort() {
        int lastHeapIndex = size - 1;
        for (int i = 0; i < lastHeapIndex; i++) {
            int temp = heap[0]; //largest value
            heap[0] = heap[lastHeapIndex - i];
            heap[lastHeapIndex - i] = temp; //exchanging root with last item in the heap (heap is being reduced by 1 on each iteration)

            // always start at root as we swap it each time
            fixHeapBelow(0, lastHeapIndex -i -1); // exclude where we have put the root
        }
    }

    public void printHeap() {
        for (int i = 0; i < size; i++) {
            System.out.print(heap[i]);
            System.out.print(", ");
        }
        System.out.println();
    }

    private void fixHeapBelow(int index, int lastHeapIndex) { //index = index of item we deleted
        int childToSwap;

        while (index <= lastHeapIndex) {
            int leftChild = getChild(index, true);
            int rightChild = getChild(index, false);

            if (leftChild <= lastHeapIndex) { //this node has a left child (remember can't have a node with no left child and right child as heaps need to be complete trees)
                if (rightChild > lastHeapIndex) { // does not have right child as it is > last index
                    childToSwap = leftChild;
                } else {
                    childToSwap = (heap[leftChild] > heap[rightChild] ? leftChild : rightChild);
                }

                if (heap[index] < heap[childToSwap]) {
                    int temp = heap[index];
                    heap[index] = heap[childToSwap];
                    heap[childToSwap] = temp;
                } else {
                    break;
                }

                index = childToSwap;
            } else {
                break;
            }
        }
    }

    private void fixHeapAbove(int index) {
        int newValue = heap[index];

        // while not root, and the new value to insert is > the parent nodes value
        while (index > 0 && newValue > heap[getParent(index)]) {
            heap[index] = heap[getParent(index)];
            index = getParent(index); // any parents < new value are shifting down here
        }

        heap[index] = newValue; //assign new value in correct position
    }

    public boolean isFull() {
        return size == heap.length;
    }

    public int getParent(int index) {
        return (index - 1) / 2;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int getChild(int index, boolean left) {
        return 2 * index + (left ? 1 : 2);
    }
}
