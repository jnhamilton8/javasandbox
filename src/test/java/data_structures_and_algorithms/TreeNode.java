package data_structures_and_algorithms;

public class TreeNode {

    private int data;
    private TreeNode leftChild;
    private TreeNode rightChild;

    public TreeNode(int data) {
        this.data = data;
    }

    public TreeNode get(int value) {
        if (value == data) {
            return this; //found the node we are looking for
        }

        if (value < data) {
            if (leftChild != null) {
                return leftChild.get(value); // look for value in left subtree recursively
            }
        } else {
            if (rightChild != null) {
                return rightChild.get(value); // look at right subtree
            }
        }
        return null; // couldn't find the value
    }

    public int min() {
        if (leftChild == null) {
            return data; //we have traversed all the way to the left of the tree so that final leaf must be the min
        } else {
            return leftChild.min(); //travel down left edges until hit a node with null child, and we have hit the minimum
        }
    }

    public int max() {
        if (rightChild == null) {
            return data; //we have traversed all the way to the right of the tree so that final leaf must be the max
        } else {
            return rightChild.max(); //travel down right edges until hit a node with null child, and we have hit the maximum
        }
    }

    public void insert(int value) {
        if (value == data) {
            return; //do not support duplicate values
        }

        if (value < data) {
            if (leftChild == null) { //found where we want to insert
                leftChild = new TreeNode(value);
            } else {
                leftChild.insert(value);
            }
        } else {
            if (rightChild == null) {
                rightChild = new TreeNode(value);
            } else {
                rightChild.insert(value);
            }
        }
    }

    public void traverseInOrder() {
        if (leftChild != null) { //traverse left first
            leftChild.traverseInOrder();
        }

        System.out.print(data + ", ");

        if (rightChild != null) { //then traverse right
            rightChild.traverseInOrder();
        }
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                '}';
    }
}
