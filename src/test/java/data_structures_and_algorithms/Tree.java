package data_structures_and_algorithms;

public class Tree {

    private TreeNode root;

    public void insert(int value) {
        if (root == null) {
            root = new TreeNode(value); //empty tree, insert at root
        } else {
            root.insert(value);
        }
    }

    public void traverseInorder() {
        if (root != null) {
            root.traverseInOrder();
        }
    }

    public TreeNode get(int value) {
        if (root != null) {
            return root.get(value);
        }
        return null; // empty tree so the value cannot be there
    }

    public void delete(int value) {
        root = delete(root, value); //passing root of subtree we want to search for the value
    }

    private TreeNode delete(TreeNode subTreeRoot, int value) {
        if (subTreeRoot == null) {
            return null;
        }

        if (value < subTreeRoot.getData()) {
            subTreeRoot.setLeftChild(delete(subTreeRoot.getLeftChild(), value)); // if the value < root of subtree we are searching we want to move to the subtree's left child
        } else if (value > subTreeRoot.getData()) {
            subTreeRoot.setRightChild(delete(subTreeRoot.getRightChild(), value)); //as above, look down right subtree
        } else {
            // Cases 1 and 2: node to delete has 0 or 1 child(ren)
            if (subTreeRoot.getLeftChild() == null) { // if leaf, will return null
                return subTreeRoot.getRightChild(); //right child replaces that of parent
            } else if (subTreeRoot.getRightChild() == null) {
                return subTreeRoot.getLeftChild(); // if no right or left child just return null and lob off subTreeRoot
            }

            // Case 3: node to delete has 2 children
            subTreeRoot.setData(subTreeRoot.getRightChild().min()); //find the min value in right subtree and replace value of node we are deleting with that value
            subTreeRoot.setRightChild(delete(subTreeRoot.getRightChild(), subTreeRoot.getData())); // delete the node that has the smallest value in the right subtree
        }

        return subTreeRoot; //means this node is not the node we want to delete. so just return it
    }

    public int min() {
        if (root == null) {
            return Integer.MIN_VALUE; //empty tree - return smallest value can store in an integer
        } else {
            return root.min();
        }
    }

    public int max() {
        if (root == null) {
            return Integer.MAX_VALUE; //empty tree - return largest value can store in an integer
        } else {
            return root.max();
        }
    }
}
