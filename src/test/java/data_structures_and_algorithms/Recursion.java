package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class Recursion {

    @Test
    void canCalculateFactorialIteratively() {
        int result = iterativeFactorial(6);

        assertThat(result, equalTo(720));
    }

    @Test
    void canCalculateFactorialRecursively() {
        int result = iterativeFactorial(6);

        assertThat(result, equalTo(720));
    }

    // 1! = 1 * 0 = 1
    // 2! = 2 * 1 = 2 * 1!
    // 3! = 3 * 2 * 1 = 3 * 2!
    // 4! = 4 * 3 * 2 * 1 = 4 * 3!
    // n! = n *(n - 1)!

    private int iterativeFactorial(int num) {
        if (num == 0) {
            return 1;
        }

        int factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        return factorial;
    }

    private int recursiveFactorial(int num){
        if (num == 0) {
            return 1;
        }

        return num * recursiveFactorial(num - 1);
    }
}
