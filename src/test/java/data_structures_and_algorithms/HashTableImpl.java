package data_structures_and_algorithms;

import org.junit.jupiter.api.Test;

public class HashTableImpl {

    @Test
    void newHashTableOperations() {
        HashTable hashTable = new HashTable();
        hashTable.put("Jones", new Employee("Jane", "Jones", 123));
        hashTable.put("Doe", new Employee("John", "Doe", 4567));
        hashTable.put("Smith", new Employee("Mary", "Smith", 22));
        hashTable.put("Wilson", new Employee("Mike", "Wilson", 3245));

        hashTable.printHashTable();

        System.out.println("Retrieve key Wilson: " + hashTable.get("Wilson"));
        System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));

        hashTable.remove("Wilson");
        hashTable.remove("Jones");

        hashTable.printHashTable();

        System.out.println("Retrieve key Smith: " + hashTable.get("Smith"));
    }

    public static class HashTable {

        private StoredEmployee[] hashTable;

        public HashTable() {
            hashTable = new StoredEmployee[10];
        }

        public void put(String key, Employee employee) {
            int hashedKey = hashKey(key);
            if (occupied(hashedKey)) { //if occupied we need to do linear probing
                int stopIndex = hashedKey;

                if (hashedKey == hashTable.length - 1) {
                    hashedKey = 0;
                } else {
                    hashedKey++;
                }

                while (occupied(hashedKey) && hashedKey != stopIndex) {
                    hashedKey = (hashedKey + 1) % hashTable.length; //wraps hashedKey back to beginning of array when reach end of array
                }
            }

            if (occupied(hashedKey)) { //if still occupied after all that...
                System.out.println("Sorry there is already an employee at position: " + hashedKey);
            } else {
                hashTable[hashedKey] = new StoredEmployee(key, employee);
            }
        }

        public Employee get(String key) {
            int hashedKey = findKey(key);
            if (hashedKey == -1) {
                return null;
            } else {
                return hashTable[hashedKey].employee;
            }
        }

        public Employee remove(String key) {
            int hashedKey = findKey(key);
            if (hashedKey == -1) {
                return null;
            }

            Employee employee = hashTable[hashedKey].employee;
            hashTable[hashedKey] = null; //null out the removed employee in the hastable

            // rehash the items in the hashtable, so that there are no null values between entries, meaning we cannot find certain employees
            StoredEmployee[] oldHashTable = hashTable;
            hashTable = new StoredEmployee[oldHashTable.length];

            for (int i = 0; i < oldHashTable.length; i++) {
                if (oldHashTable[i] != null) {
                    put(oldHashTable[i].key, oldHashTable[i].employee);
                }
            }
            return employee;
        }

        private int hashKey(String key) {
            return key.length() % hashTable.length; //modding by length of hashtable so will get keys from 0-9
        }

        private int findKey(String key) {
            int hashedKey = hashKey(key);
            if (hashTable[hashedKey] != null && hashTable[hashedKey].key.equals(key)) {
                return hashedKey;
            } // if we don't find the key in the basic hashed slot we need to do linear probing

            int stopIndex = hashedKey;

            if (hashedKey == hashTable.length - 1) {
                hashedKey = 0;
            } else {
                hashedKey++;
            }

            while (hashedKey != stopIndex && hashTable[hashedKey] != null && !hashTable[hashedKey].key.equals(key)) {
                hashedKey = (hashedKey + 1) % hashTable.length; //wraps hashedKey back to beginning of array when reach end of array
            }

            if (hashTable[hashedKey] != null && hashTable[hashedKey].key.equals(key)) {
                return hashedKey;
            } else {
                return - 1;
            }
        }

        private boolean occupied(int index) {
            return hashTable[index] != null;
        }

        public void printHashTable() {
            for (StoredEmployee employee : hashTable) {
                System.out.println(employee);
            }
        }
    }
}
