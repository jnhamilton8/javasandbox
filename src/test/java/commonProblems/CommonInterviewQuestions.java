package commonProblems;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class CommonInterviewQuestions {

    @Test
    void test_return_true_if_number_is_prime() {
        var isPrime = isNumberPrime(5);
        assertThat(isPrime, is(true));
    }

    @Test
    void test_return_false_if_number_is_not_prime() {
        var isPrime = isNumberPrime(10);
        assertThat(isPrime, is(false));
    }

    @Test
    void test_return_true_if_string_is_palindrome() {
        var isPalindrome = isStringAPalindrome("abba");
        assertThat(isPalindrome, is(true));
    }

    @Test
    void test_return_false_if_string_is_not_palindrome() {
        var isPalindrome = isStringAPalindrome("testing");
        assertThat(isPalindrome, is(false));
    }

    @Test
    void test_return_false_if_string_is_empty_and_not_a_palindrome() {
        var isPalindrome = isStringAPalindrome("");
        assertThat(isPalindrome, is(false));
    }

    @Test
    void test_returns_true_for_one_char_string_as_palindrome() {
        var isPalindrome = isStringAPalindrome("c");
        assertThat(isPalindrome, is(true));
    }

    @Test
    void test_returns_correct_factorial_of_number() {
        var factorial = returnFactorial(10);
        assertThat(factorial, equalTo(3628800L));
    }

    @Test
    void test_correctly_return_fibonacci_up_to_10() {
        var fibList = returnFibonacciOfNumberUpTo(10);
        assertThat(fibList, equalTo(List.of(1, 1, 2, 3, 5, 8, 13, 21, 34, 55)));
    }

    /**
     * Confirms if a number is a prime number
     * <p>
     * A prime number is a number that is divisible by only two numbers: 1 and itself.
     * So, if any number is divisible by any other number, it is not a prime number.
     *
     * @param num the number, as an int
     * @return true if the number is a prime, false otherwise
     */
    public boolean isNumberPrime(int num) {
        boolean flag = true;

        for (int i = 2; i <= num / 2; ++i) { //divide by two as if a number is higher than half the number you're checking there's no way you can multiply it by any integer and get the number you're looking for
            if (num % i == 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * Checks to see whether a string is a palindrome i.e.a word, phrase, or sequence that reads the same backwards as forwards
     *
     * @param aString the string to test
     * @return true if the word is a palindrome, false otherwise
     */
    public boolean isStringAPalindrome(String aString) {
        StringBuilder reversedStr = new StringBuilder();

        if (aString.isBlank()) {
            return false;
        }

        for (int i = aString.length() - 1; i >= 0; --i) {
            reversedStr.append(aString.charAt(i));
        }

        return aString.equalsIgnoreCase(reversedStr.toString());
    }

    /**
     * Calculates the factorial of a number - which is to multiply all whole numbers from the chosen number down to 1.
     *
     * @param aNumber the number to return the factorial for
     * @return the calculated factorial
     */
    public long returnFactorial(int aNumber) {
        long factorial = 1;

        for (int i = 1; i <= aNumber; ++i) {
            factorial *= i;
        }
        return factorial;
    }

    /**
     * Calculates and returns the fibonacci sequence up to "number"
     * fn = fn-1 + fn-2. The first two numbers of the Fibonacci series are always 1, 1.
     *
     * @param number the number which to return the Fibonacci sequence up to
     * @return the Fibonacci sequence
     */
    public List<Integer> returnFibonacciOfNumberUpTo(int number) {
        List<Integer> fibonacciNumbers = new ArrayList<>(Arrays.asList(1, 1));
        int num1 = 1;
        int num2 = 1;
        int fibonacci;

        for (int i = 3; i <= number; i++) {
            fibonacci = num1 + num2;
            fibonacciNumbers.add(fibonacci);

            num1 = num2;
            num2 = fibonacci;
        }
        return fibonacciNumbers;
    }
}
