package commonProblems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StringsTest {

    Strings strings;

    @BeforeEach
    void beforeEach() {
        strings = new Strings();
    }

    @Test
    void isUniqueChars_returns_true_when_string_contains_unique_characters() {
        boolean result = strings.isUniqueChars("abcde");

        assertThat(result, is(true));
    }

    @Test
    void isUniqueChars_returns_false_when_string_contains_duplicate_characters() {
        boolean result = strings.isUniqueChars("aacde");

        assertThat(result, is(false));
    }

    @Test
    void permutation_returns_true_when_first_string_is_permutation_of_second_string_and_of_same_length() {
        String string1 = "acb";
        String string2 = "acb";

        boolean result = strings.permutation(string1, string2);
        assertThat(result, is(true));
    }

    @Test
    void permutation_returns_false_when_strings_are_of_different_lengths() {
        String string1 = "acb";
        String string2 = "acbt";

        boolean result = strings.permutation(string1, string2);
        assertThat(result, is(false));
    }

    @Test
    void permutation_returns_false_when_first_string_is_not_a_permutation_of_second_string_and_of_same_length() {
        String string1 = "acb";
        String string2 = "aeb";

        boolean result = strings.permutation(string1, string2);
        assertThat(result, is(false));
    }

    @Test
    void replaceSpaces_returns_original_string_with_spaces_replaced_by_replacement_string() {
        String originalString = "Mr John Smith    ";
        int trueLength = 13;

        String amendedString = strings.replaceSpaces(originalString.toCharArray(), trueLength);
        assertThat(amendedString, equalTo("Mr%20John%20Smith"));
    }

    @Test
    void isPermutationOfPalindrome_returns_true_when_string_is_a_permutation_of_a_palindrome(){
        String phrase = "Tact Coa";

        boolean result = strings.isPermutationOfPalindrome(phrase);
        assertThat(result, is(true));
    }

    @Test
    void isPermutationOfPalindrome_returns_false_when_string_is_not_a_permutation_of_a_palindrome(){
        String phrase = "animal";

        boolean result = strings.isPermutationOfPalindrome(phrase);
        assertThat(result, is(false));
    }

    @Test
    void one_edit_away_returns_true_when_two_strings_are_one_replace_away() {
        String one = "pale";
        String two = "bale";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(true));
    }

    @Test
    void one_edit_away_returns_true_when_two_strings_are_one_removal_away() {
        String one = "pale";
        String two = "ple";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(true));
    }

    @Test
    void one_edit_away_returns_true_when_two_strings_are_one_insertion_away() {
        String one = "ple";
        String two = "pale";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(true));
    }

    @Test
    void one_edit_away_returns_true_when_two_strings_are_the_same() {
        String one = "same";
        String two = "same";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(true));
    }

    @Test
    void one_edit_away_returns_false_when_two_strings_are_of_different_lengths() {
        String one = "different";
        String two = "differentlengths";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(false));
    }

    @Test
    void one_edit_away_returns_false_when_two_strings_are_more_than_one_replace_away() {
        String one = "pale";
        String two = "bele";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(false));
    }

    @Test
    void one_edit_away_returns_false_when_two_strings_are_more_than_one_insertion_away() {
        String one = "pe";
        String two = "pale";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(false));
    }

    @Test
    void one_edit_away_returns_false_when_two_strings_are_more_than_one_removal_away() {
        String one = "pale";
        String two = "pe";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(false));
    }

    @Test
    void one_edit_away_returns_true_when_two_strings_are_empty() {
        String one = "";
        String two = "";

        boolean actualresult = strings.oneEditAway(one, two);
        assertThat(actualresult, is(true));
    }

    @Test
    void compress_returns_compressed_string_if_it_is_shorter_length_than_original_string() {
        String str = "aabcccccaaa";

        String actualResult = strings.compress(str);
        assertThat(actualResult, equalTo("a2b1c5a3"));

    }

    @Test
    void compress_returns_original_string_if_compressed_string_is_not_shorter_than_original() {
        String str = "abcd";

        String actualResult = strings.compress(str);
        assertThat(actualResult, equalTo("abcd"));
    }

    @Test
    void compress_returns_original_string_if_original_string_is_empty() {
        String str = "";

        String actualResult = strings.compress(str);
        assertThat(actualResult, equalTo(""));
    }

    @Test
    void rotate_returns_NxN_matrix_rotated_90_degrees() {
        int matrix[][] = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 34, 25}
        };

        int expected[][] = {
                {21, 16, 11, 6, 1},
                {22, 17, 12, 7, 2},
                {23, 18, 13, 8, 3},
                {34, 19, 14, 9, 4},
                {25, 20, 15, 10, 5}
        };

        int actual[][] = strings.rotate(matrix);
        System.out.println(Arrays.deepToString(actual));
        assertThat(actual, equalTo(expected));
    }

    @Test
    void rotate_throws_illegalArgumentException_when_matrix_is_not_a_square() {
        int matrix[][] = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9,},
                {10, 11, 12}
        };

        IllegalArgumentException e = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> strings.rotate(matrix));

        assertThat(e.getMessage(), equalTo("Matrix must be a square"));
    }

    @Test
    void seZeros_sets_row_and_column_of_matrix_to_zero_if_there_is_an_element_with_value_of_zero() {
        int matrix[][] = {
                {1, 0, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };

        int expected[][] = {
                {0, 0, 0, 0},
                {5, 0, 7, 8},
                {9, 0, 11, 12}
        };


        int actual[][] = strings.setZeros(matrix);
        System.out.println(Arrays.deepToString(actual));
        assertThat(actual, equalTo(expected));
    }

    @Test
    void seZeros_leaves_the_matrix_unchanged_if_it_does_not_contain_any_zeros() {
        int matrix[][] = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 112}
        };

        int actual[][] = strings.setZeros(matrix);
        assertThat(actual, equalTo(matrix));
    }

    @Test
    void isRotation_returns_true_when_string2_is_rotation_of_string1() {
        String s1 = "waterbottle";
        String s2 = "erbottlewat";

        boolean actual = strings.isRotation(s1, s2);
        assertThat(actual, is(true));
    }

    @Test
    void isRotation_returns_false_when_string2_is_not_a_rotation_of_string1() {
        String s1 = "hello";
        String s2 = "round";

        boolean actual = strings.isRotation(s1, s2);
        assertThat(actual, is(false));
    }

    @Test
    void isRotation_returns_false_when_strings_are_of_different_length() {
        String s1 = "waterbottle";
        String s2 = "waterbottlee";

        boolean actual = strings.isRotation(s1, s2);
        assertThat(actual, is(false));
    }

    @Test
    void isRotation_returns_false_when_both_strings_are_empty() {
        String s1 = "";
        String s2 = "";

        boolean actual = strings.isRotation(s1, s2);
        assertThat(actual, is(false));
    }

    @Test
    void isRotation_returns_false_when_one_string_is_empty() {
        String s1 = "test";
        String s2 = "";

        boolean actual = strings.isRotation(s1, s2);
        assertThat(actual, is(false));
    }
}