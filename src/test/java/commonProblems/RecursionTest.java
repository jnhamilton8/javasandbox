package commonProblems;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RecursionTest {

    Recursion recursion;

    @BeforeEach
    void beforeEach() {
        recursion = new  Recursion();
    }

    @Test
    void testSimpleRecursion(){ recursion.simpleRecursiveMethod(4);
    }

    @Test
    void testFactorialRecursion(){
        assertEquals(recursion.factorialRecursion(5), 120);
        assertEquals(recursion.factorialRecursion(1), 1);
    }
}