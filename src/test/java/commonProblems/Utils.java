package commonProblems;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class Utils {

    public boolean isBigDecimalCloseTo(BigDecimal actual, BigDecimal expected, BigDecimal delta) {
        BigDecimal difference = actual.subtract(expected).abs().setScale(2, RoundingMode.HALF_UP); //get the absolute difference bet. expected and actual
        return difference.compareTo(delta) < 0 || difference.compareTo(delta) == 0;  //compare difference with tolerance, true if difference < or = tolerance
    }

    public boolean isFloatCloseTo(float actual, float expected, float delta) {
        return Math.abs(actual - expected) < delta;
    }

    public boolean isBigDecimalZero(BigDecimal decimal) {
        return decimal.compareTo(BigDecimal.ZERO) == 0;
    }

    @Test
    public void isBigDecimalZero_returns_false_when_value_with_precision_is_not_zero() {
        BigDecimal decimal = new BigDecimal("0.00000000000000001");

        boolean result = isBigDecimalZero(decimal);
        assertThat(result, is(false));
    }

    @Test
    public void isBigDecimalZero_returns_true_when_value_with_precision_is_zero() {
        BigDecimal decimal = new BigDecimal("0.000000000000");

        boolean result = isBigDecimalZero(decimal);
        assertThat(result, is(true));
    }

    @Test
    public void isBigDecimalCloseTo_returns_true_when_difference_less_than_delta() {
        BigDecimal expected = new BigDecimal("12.3644");
        BigDecimal actual = new BigDecimal("12.4000");

        boolean result = isBigDecimalCloseTo(actual, expected, new BigDecimal("0.05"));

        assertThat(result, is(true));
    }

    @Test
    public void isBigDecimalCloseTo_returns_true_when_difference_equals_delta() {
        BigDecimal expected = new BigDecimal("12.3544");
        BigDecimal actual = new BigDecimal("12.4000");

        boolean result = isBigDecimalCloseTo(actual, expected, new BigDecimal("0.05"));

        assertThat(result, is(true));
    }

    @Test
    public void isBigDecimalCloseTo_returns_false_when_difference_greater_than_delta() {
        BigDecimal expected = new BigDecimal("12.3444");
        BigDecimal actual = new BigDecimal("12.4000");

        boolean result = isBigDecimalCloseTo(actual, expected, new BigDecimal("0.05"));

        assertThat(result, is(false));
    }

    @Test
    public void isFloatCloseTo_returns_true_when_difference_less_than_delta() {
        float expected = 12.36f;
        float actual = 12.40f;

        boolean result = isFloatCloseTo(actual, expected, 0.05f);

        assertThat(result, is(true));
    }

    @Test
    public void isFloatCloseTo_returns_true_when_difference_equals_delta() {
        float expected = 12.35f;
        float actual = 12.40f;

        boolean result = isFloatCloseTo(actual, expected, 0.05f);

        assertThat(result, is(true));
    }

    @Test
    public void isFloatCloseTo_returns_false_when_difference_greater_than_delta() {
        float expected = 12.34f;
        float actual = 12.40f;

        boolean result = isFloatCloseTo(actual, expected, 0.05f);

        assertThat(result, is(false));
    }
}