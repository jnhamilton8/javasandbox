package interview;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Interview {

    @Test
    public void verifyPersonGetCommonLastNamesReturnsMapOfLastNameAndPeopleWithSameNameUsingSet() {
        Person person1 = new Person("Jane", "Hamilton", "1");
        Person person2 = new Person("Jane", "Smith", "1");
        Person person3 = new Person("Jane", "Hamilton", "1");
        Person person4 = new Person("Jane", "Jones", "1");
        Person person5 = new Person("Jane", "Hamilton", "1");

        var listOfPeople = new ArrayList<Person>();
        listOfPeople.add(person1);
        listOfPeople.add(person2);
        listOfPeople.add(person3);
        listOfPeople.add(person4);
        listOfPeople.add(person5);
        var expectedReturnedList = List.of(new Person("Jane", "Hamilton", "1"),
                new Person("Jane", "Hamilton", "1"));
        var expectedReturnedMap = new HashMap<String, List<Person>>();
        expectedReturnedMap.put("Hamilton", expectedReturnedList);

        var returnedMapOfCommonLastnamePeople = getCommonLastNamesUsingSet(listOfPeople);
        assertEquals(expectedReturnedMap, returnedMapOfCommonLastnamePeople);
    }

    @Test
    public void verifyPersonGetCommonLastNamesReturnsMapOfLastNameAndPeopleWithSameNameUsingIteration() {
        Person person1 = new Person("Jane", "Hamilton", "1");
        Person person2 = new Person("Jane", "Smith", "1");
        Person person3 = new Person("Jane", "Hamilton", "1");
        Person person4 = new Person("Jane", "Jones", "1");
        Person person5 = new Person("Jane", "Hamilton", "1");

        var listOfPeople = new ArrayList<Person>();
        listOfPeople.add(person1);
        listOfPeople.add(person2);
        listOfPeople.add(person3);
        listOfPeople.add(person4);
        listOfPeople.add(person5);
        var expectedReturnedList = List.of(new Person("Jane", "Hamilton", "1"),
                new Person("Jane", "Hamilton", "1"), new Person("Jane", "Hamilton", "1"));
        var expectedReturnedMap = new HashMap<String, List<Person>>();
        expectedReturnedMap.put("Hamilton", expectedReturnedList);

        var returnedMapOfCommonLastnamePeople = getCommonLastNamesUsingIteration(listOfPeople);
        assertEquals(expectedReturnedMap, returnedMapOfCommonLastnamePeople);
    }


    /**
     * Create Person class that contains first name, last name, and ID.
     * Create a getCommonLastNames() method with that takes List as a parameter.
     * The method should return a Map<String, List> whereby the key is the common last name and the value is the list of people that share the same last name.
     */
    class Person {
        String firstName;
        String lastName;
        String id;

        Person(String firstName, String lastName, String id) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
        }

        public String getLastName() {
            return this.lastName;
        }

        public String getFirstName() {
            return this.firstName;
        }

        @Override
        public String toString() {
            return this.getFirstName() + " " + this.getLastName();
        }

        @Override
        public boolean equals(Object person) {
            if (person == this) {
                return true;
            }

            if (!(person instanceof Person)) {
                return false;
            }

            Person p = (Person) person;
            return p.getFirstName().equals(this.getFirstName()) && p.getLastName().equals(this.getLastName());
        }
    }

    /**
     * This method won't work well, as it will miss out the initial value, so only return 2 instances of Jane Hamilton Person,
     * as the first item is not in the set
     *
     * @param personList List of People
     * @return A map containing the duplicated lastname as a String, and list of people containing that last name as the value
     */
    public Map<String, List<Person>> getCommonLastNamesUsingSet(List<Person> personList) {
        var commonLastNames = new HashMap<String, List<Person>>();
        Set<String> set = new HashSet<>();
        List<Person> duplicateLastNames = new ArrayList<>();
        
        for (Person person : personList) {
            var lastName = person.getLastName();
            if (!set.add(lastName)) { //adding to a set returns false if the value already exists, so if false add to the list of duplicate names
                duplicateLastNames.add(person);
            }
        }

        var commonLastName = duplicateLastNames.get(0).getLastName(); //getting any of the names in here is fine as they are all the same
        commonLastNames.put(commonLastName, duplicateLastNames);

        return commonLastNames;
    }


    public Map<String, List<Person>> getCommonLastNamesUsingIteration(List<Person> personList) {
        var commonLastNames = new HashMap<String, List<Person>>();
        List<Person> duplicateLastNames = new ArrayList<>();

        //iterate over the list, element by element, comparing each element to all others in the list and add to duplicatesList if they match
        for (int i = 0; i < personList.size(); i++) {
            for (int j = i + 1; j < personList.size(); j++) {
                var currentPerson = personList.get(i);
                var nextPerson = personList.get(j);
                if (currentPerson.getLastName().equals(nextPerson.getLastName())) {
                    duplicateLastNames.add(currentPerson);
                }
            }
        }

        var commonLastName = duplicateLastNames.get(0).getLastName(); //getting any of the names in here is fine as they are all the same
        commonLastNames.put(commonLastName, duplicateLastNames);

        return commonLastNames;
    }
}
