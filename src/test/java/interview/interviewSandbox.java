package interview;

import org.junit.jupiter.api.Test;

import java.util.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class interviewSandbox {

    @Test
    public void testAThing() {
        // test a thing
        assertThat("", equalTo(""));
        assertThat(List.of("test"), not((empty())));
        assertThat("".isBlank(), is(true));
    }
}
