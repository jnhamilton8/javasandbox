package java8.examples;

import java.util.*;

import java8.examples.Lambdas.*;
import org.junit.jupiter.api.Test;

import static java.util.Collections.singletonList;
import static java.util.Map.entry;
import static java8.examples.Utils.printTheTesterList;
import static java8.examples.Utils.printTheTesterMap;

public class Collections {

    @Test
    public void newMethodsOnIterableCollection() {
        List<Tester> testers = new ArrayList<Tester>(Arrays.asList(
                new Tester("Jane", "Hamilton", 34),
                new Tester("Joe", "Smith", 23),
                new Tester("Alycia", "Warner", 40)));

        testers.replaceAll(Tester::firstNameToUpperCase); //takes a UnaryOperator - an operation on a single param that produces a result of the same type as the param
        testers.forEach(System.out::println);  //forEach takes a Consumer

        testers.removeIf(tester -> tester.getAge() < 30); //takes a predicate.  Will remove all objects that fit the criteria i.e. Joe.
        printTheTesterList("Joe will have been removed", testers);

        testers.sort(Comparator.comparing(Tester::getFirstName)
                .thenComparing(Tester::getAge));  //sort by firstName then by age
        printTheTesterList("Prints ALYCIA then JANE", testers);
    }

    @Test
    public void newMethodsOnMapInterface() {
        Map<Integer, Tester> testerMap = Map.ofEntries(  //creates immutable map
                entry(1, new Tester("Jane", "Hamilton", 34)),
                entry(2, new Tester("Joe", "Smith", 23))
        );

        Map<Integer, Tester> testerMap2 = new HashMap<>(); //this map is mutable
        testerMap2.put(1, new Tester("Jane", "Hamilton", 34));
        testerMap2.put(2, new Tester("Joe", "Smith", 23));

        //takes BiConsumer(takes 2 args and produces no result)
        testerMap.forEach((key, tester) ->
                System.out.println(key + " " + tester)
        );

        //checks if keys is present, if is, returns the value, if not returns default value
        Tester returnedTester = testerMap.getOrDefault(3, new Tester("Sally", "Jones", 45));
        System.out.println("Returned tester: " + returnedTester);

        //Will check if key is in map, if it is, it will do nothing, if not it will add the value
        testerMap2.putIfAbsent(3, new Tester("Sally", "Jones", 45));
        printTheTesterMap("Now with Sally added", testerMap2);

        //compute* methods
        //computes a new value from the key passed as a param, that may/may not be in the map
        //takes a key, and remapping function - the lambda will compute the remapping
        testerMap2.computeIfPresent(1, (key, tester) -> tester.firstNameToUpperCase()); //if finds key 1, replaces the testers first name to uppercase
        printTheTesterMap("Now with Jane in uppercase added", testerMap2);
    }

    //merge function
    @Test
    void letsLookAtTheMapMergeFunction() {
        List<Tester> testerList = singletonList(new Tester("Jane", "Hamilton", 34));
        List<Tester> testerList2 = singletonList(new Tester("Sally", "Jones", 39));
        Map<String, List<Tester>> testerMap1 = new HashMap<>();
        Map<String, List<Tester>> testerMap2 = new HashMap<>();
        testerMap1.put("TeamA", testerList);
        testerMap2.put("TeamB", testerList2);

        //merges (adds) the key/values from testerMap2 to testerMap1
        testerMap2.forEach(
                (team, tester) -> //for each team/tester pair in testerMap2
                        testerMap1.merge( //go ahead and merge it into testerMap1
                                team, tester, //if key is not present, the pair will be added to testerMap1 with no further processing
                                (existingTeam, newTester) -> { //go ahead and add all newTesters to the existingTeam list
                                    existingTeam.addAll(newTester);
                                    return existingTeam; //return the updated existingTeam with the newly added testers
                                }
                        )
        );

        printTheTesterMap("Should contain a list of TeamA and TeamB with Jane and Sally as merged them into testerMap1", testerMap1);
    }
}
