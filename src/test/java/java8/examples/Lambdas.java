package java8.examples;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.*;

import static java8.examples.Utils.printArray;
import static java8.examples.Utils.printTheTesterList;

public class Lambdas {
    /*Lambdas and Functional Interfaces A Lambda expression (or function) is an anonymous function i.e. a function with no name
    and not being bound to any identifier. They are nameless functions written where they are needed, often as a parameter to another
    function. Functional interfaces are interfaces with one single method.A Lambda expression is an instance of a Functional Interface
    - which Functional Interface it implements is deduced from the context in which it is used  */

    @Test
    void firstLookAtLambdasWithComparatorAndRunnable() {
        //Lambdas allow us to pass code as a parameter (instead of using anonymous classes)
        Comparator<String> comparator = (s1, s2) -> Integer.compare(s1.length(), s2.length());

        //Interestingly, can actually now write the above like this
        Comparator<String> comparator2 = Comparator.comparingInt(String::length).reversed();

        String[] testers = {"Jane", "Jim", "Harry"};

        Arrays.sort(testers, comparator);
        printArray("Array sorted by length asc ", testers);
        Arrays.sort(testers, comparator2);
        printArray("Array sorted by length dec ", testers);

        //If we have more than one line we need to keep {}
        Runnable r = () -> {
            int i = 0;
            while (i++ < 10) {
                System.out.println("It works!");
            }
        };

        r.run(); //print it works!
    }

    @Test
    public void iterateOverAListUsingLambda() {
        List<String> aList = new ArrayList<>();
        aList.add("1");
        aList.add("2");

        aList.forEach(System.out::println);
    }

    /*
    The interface for Runnable is:
    @FunctionalInterface
    public interface Runnable {
        public abstract void run();
    }
    Below, we pass a lambda expression to constructor of Thread class.
    The Thread constructor takes a Runnable param.
    Runnable is a Functional Interface with one method, Run
    So the compiler will try and convert the below lambda into suitable Run code
    and execute

    It was:
    new Thread(new Runnable() {
        @Override
         public void run() {
             System.out.println("This is my runnable!");
         }
    }).start();
    */

    @Test
    public void createANewRunnableUsingLambda() {
        new Thread(
                () -> System.out.println("This is my runnable!")
        ).start();
    }

    @Test
    public void sortingObjectsUsingLambda() {
        Tester[] testers = {
                new Tester("Jane", "Hamilton", 10),
                new Tester("Jim", "Smith", 21),
                new Tester("Harry", "Johnson", 43)
        };

        Arrays.sort(testers, Tester::nameCompare);
        printArray("Sorted array by firstname asc", testers);
    }

    /*
    Method References

    Alternative syntax for lambda expressions when using Functional Interfaces
    Method reference is NOT a static call, it is a lambda expression
    Functional Interfaces only one method
    */

    @Test
    public void lookingAtMethodReferencesAndFunctionalInterfaces() {
        //Function<Tester, String> f = person -> person.getAge();

        //Function accepts one arg and produces a result
        Function<Tester, String> f = Tester::getFirstName;
        System.out.println(f.apply(new Tester("Jane", "Hamilton", 10)));  //prints out "Jane"

        //BinaryOperator<Integer> sum = (i1, i2) -> Integer.sum(i1, i2);
        BinaryOperator<Integer> sum = Integer::sum;
        System.out.println(sum.apply(2, 5)); //prints out 7

        //Consumer takes a single param and returns no result, operates via it's side effects
        Consumer<String> printer = System.out::println;
        printer.accept("test"); //will print out "test" to the console

        //Predicate represents a boolean valued function
        //Usually used to filter out things in lists/maps
        String aRandomString = RandomStringUtils.randomAlphabetic(10);
        Predicate<String> p1 = s -> s.length() < 20;
        if (p1.test(aRandomString)) {
            System.out.println("Whooooo I'm less than 20! " + aRandomString);
        } else {
            System.out.println("I am too long :(");
        }

        Predicate<String> p2 = s -> s.length() > 5;
        Predicate<String> p3 = p1.and(p2); //chaining predicates. Logical AND condition
        System.out.println(p3.test("It is a nice day")); //will print true, it is < 20 and > 5 chars
        Predicate<String> p4 = p1.or(p2); //logical OR condition
        System.out.println(p4.test("morethan20butimgonnabemorethan5")); //will print true as not < 20 but is > 5

        //Supplier returns an object but takes no parameter
        Supplier<Tester> testerSupplier = Tester::new;
        Tester aTester = testerSupplier.get();
        System.out.println(aTester); //I will print the default names and 18
    }

    @Test
    public void lookingAtComparatorsAndLambdas() {
        Tester tester1 = new Tester("Jane", "Hamilton", 10);
        Tester tester2 = new Tester("Jane", "Smith", 21);

        List<Tester> testerList = Arrays.asList(tester1, tester2);

        //Create a comparator that firstly compares by lastName, and firstName, then age
        Comparator<Tester> comp = Comparator.comparing(Tester::getLastName)
                .thenComparing(Tester::getFirstName)
                .thenComparing(Tester::getAge);

        //Go ahead and use this comparator to compare to tester objects.  It will compare by lastName as firstName is the same
        System.out.println(comp.compare(tester1, tester2)); //prints negative int, as Hamilton is less than Smith
        //Can also use this comparator to sort a list of Tester objects
        testerList.sort(comp);
        printTheTesterList("Sorted list by lastname as firstname is the same ", testerList);
    }
}
