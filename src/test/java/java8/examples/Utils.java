package java8.examples;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Utils {

    public static <K, V> void printTheTesterMap(String message, Map<K, V> testers) {
        System.out.println(message);
        for (Map.Entry<K, V> entry : testers.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public static <T> void printTheTesterList(String message, List<T> testers) {
        System.out.println(message);
        for (T tester : testers) {
            System.out.println(tester);
        }
    }

    public static <T> void printArray(String message, T[] array) {
        System.out.println(message);
        System.out.println(Arrays.toString(array));
    }
}
