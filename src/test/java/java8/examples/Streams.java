package java8.examples;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {

    /*
    A stream is a typed interface, can be defined on any typed object.
    It is a data structure BUT it doesn't hold any data - it processes it from a source
    It doesn't modify the data it processes.
    When it is used once, it is closed, and needs to be recreated.
    Consider using a Stream instead of the usual collection, can do lots of cool stuff
    with it!
     */

    @Test
    void canSuccessfullyCreateStreams() {
        Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5, 6, 7);
        intStream.forEach(System.out::println); //can use lambdas on streams

        Stream<String> stringStream = Stream.of("Hello", "this", "is", "cool");
        stringStream.forEach(System.out::println);

        List<Double> doubleList = new ArrayList<>(Arrays.asList(23.6, 578.9, 1.678));
        Stream<Double> doubleStream = doubleList.stream(); //can create from existing lists etc.
        doubleStream.forEach(System.out::println);

        //Generate creates infinite stream of the supplier. So you can use limit to restrict
        //the number and it will then terminate
        Stream<Date> dateStream = Stream.generate(Date::new).limit(10);
        dateStream.forEach(System.out::println);

        //Iterate takes a seed, and a UnaryOperator (takes element and returns element of same type)
        Stream<String> streamOfStrings = Stream.iterate("+", s -> s + "+"); //returns growing stream of pluses!
        streamOfStrings.limit(5).forEach(System.out::println); //need to limit it, as otherwise it will be infinite

        //Random generator that is local for each thread
        IntStream streamOfInt = ThreadLocalRandom.current().ints(); //returns a unlimited int stream of pseudorandom values
        streamOfInt.limit(6).forEach(System.out::println); //let's print the random ints, limiting to 6
    }

    @Test
    void canSuccessfullyConvertSteamsToLists() {
        List<Integer> aList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
        Stream<Integer> aStream = aList.stream();
        List<Integer> evenNumbersList = aStream.filter(i -> i % 2 == 0).collect(Collectors.toList());
        System.out.println("Even List: " + evenNumbersList);

        //When a steam is operated on, it closes, so we need to create a new stream
        Stream<Integer> anotherStream = aList.stream();
        Integer[] evenNumArr = anotherStream.filter(i -> i % 2 == 0).toArray(Integer[]::new);
        System.out.println("Even Array: " + Arrays.toString(evenNumArr));
    }

    public List<String> createStringList() {
        List<String> aList = new ArrayList<>();
        aList.add("Jane");
        aList.add("Patrick");
        aList.add("Alycia");
        aList.add("Clarke");
        aList.add("Ryan");
        aList.add("Jason");
        aList.add("Angelina");
        aList.add("Ruke");

        return aList;
    }

    @Test
    void canSuccessfullyUseStreamOperations() {
        List<String> memberNames = createStringList();

        //filter - returns the stream itself, is an immediate operation. Takes a predicate to filter all elements of stream
        memberNames.stream().filter(s -> s.startsWith("A")).forEach(System.out::println);

        //map - immediate operation, converts each element to another object via the given function
        memberNames.stream().sorted().map(String::toUpperCase).forEach(System.out::println);

        //forEach - is terminal operation, and returns a result
        //It iterates over the elements and performs some operation on them.
        memberNames.forEach(System.out::println);

        //collect - used to receive elements in a stream and store them in a collection
        List<String> memNamesInUpperCase = memberNames.stream().sorted()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println("Using Collect" + memNamesInUpperCase);

        //match - check whether a certain predicate matches the stream
        //Is also a terminal operation
        boolean matchedResult = memberNames.stream().anyMatch(s -> s.startsWith("A"));
        System.out.println("Using match: " + matchedResult);

        //count - terminal operation - returns number of elements in the stream as a long
        long itemsInStream = memberNames.stream().filter(s -> s.startsWith("A")).count();
        System.out.println("The number of items matching the filter is: " + itemsInStream);

        //reduce - terminal operation - reduces the elements in the stream with given function
        //Returns an optional
        Optional<String> reduced = memberNames.stream().reduce((s1, s2) -> s1 + '#' + s2);
        reduced.ifPresent(System.out::println);

        //streams the int values from the string and prints them out
        "hello".chars().forEach(System.out::println);
    }

    @Test
    void mapFilterReduceUsingStreams() {
        //compute the average age of our testers, older than 20
        List<Tester> testerList = new ArrayList<>(Arrays.asList(
                new Tester("Jane", "Hamilton", 34),
                new Tester("Sally", "Jones", 39),
                new Tester("Alycia", "Herbert", 45),
                new Tester("Joe", "Kilburn", 23),
                new Tester("Jason", "Green", 15),
                new Tester("Tim", "Smith", 19)
        ));

        //firstly let's print out the ages
        testerList.stream()
                .map(Tester::getAge)
                .peek(age -> System.out.println("Ages: " + age)) //we can peek! Use this if want to see contents of steam in intermediate stage
                .filter(age -> age > 20)
                .forEach(age -> System.out.println("Filtered age: " + age));

        //we cam also skip elements in a stream and skip elements too
        testerList.stream()
                .skip(2) //skip the first two elements of the stream
                .limit(3) //keep the next 3 elements of the stream
                .filter(person -> person.getAge() < 20)
                .forEach(age -> System.out.println("Filtered age: " + age)); //will return Jason Green, as we only kept next 3 in the list :)

        //Not let's go ahead and find the average age in our testerlist for those > 20
        Double averageAge = testerList.stream()
                .map(Tester::getAge) //mapping step takes a list of persons and returns stream of ints.  Not change size of list
                .filter(age -> age > 20) //filters out those testers < 20, does not change type of list it processes, but does change size of list
                .mapToDouble(val -> val).average().orElse(0.0); //maps each testers age to a double and computes the average
        //average is a special case of reduce.  Reduce completely changes the type, it's like a sql aggregation

        System.out.println(averageAge);

        Integer totalAge = testerList.stream()
                .map(Tester::getAge)
                .filter(age -> age > 20)
                .reduce(Integer::sum)
                .orElse(0);

        System.out.println(totalAge);
    }
}