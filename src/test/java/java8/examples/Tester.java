package java8.examples;

public class Tester {
    private String firstName;
    private String lastName;
    private int age;

    public Tester(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Tester() {
        this("DefaultFirstName", "DefaultLastName", 18);
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String aName) {
        this.firstName = aName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int nameCompare(Tester t1, Tester t2) {
        return t1.firstName.compareTo(t2.firstName);
    }

    public Tester firstNameToUpperCase() {
        this.setFirstName(getFirstName().toUpperCase());
        return this;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.getLastName() + " " + this.getAge();
    }
}
