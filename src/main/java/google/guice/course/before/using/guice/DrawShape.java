package google.guice.course.before.using.guice;

public interface DrawShape {

    void draw();
}
