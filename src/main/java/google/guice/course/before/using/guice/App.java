package google.guice.course.before.using.guice;

public class App {

    private static final String SQUARE_REQ = "SQUARE";

    public static void main(String[] args) {
        sendRequest(SQUARE_REQ);
    }

    private static void sendRequest(String squareReq) {
        if (squareReq.equals(SQUARE_REQ)) {
            DrawShape d = new DrawSquare();
            SquareRequest request = new SquareRequest(d);
            request.makeRequest();
        }
    }
}
