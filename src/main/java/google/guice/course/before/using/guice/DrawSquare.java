package google.guice.course.before.using.guice;

public class DrawSquare implements DrawShape {

    @Override
    public void draw() {
        System.out.println("Drawing Square!");
    }
}
