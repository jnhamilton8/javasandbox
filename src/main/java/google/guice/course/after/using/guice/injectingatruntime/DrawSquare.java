package google.guice.course.after.using.guice.injectingatruntime;

import static google.guice.course.after.using.guice.injectingatruntime.Constants.SQUARE;

public class DrawSquare implements DrawShape {

    @Override
    public void draw() {
        System.out.println("Drawing Square!");
    }

    @Override
    public String getShapeName() {
        return SQUARE;
    }
}
