package google.guice.course.after.using.guice;

import com.google.inject.Inject;
import google.guice.course.after.using.guice.annotations.Circle;

public class CircleRequest {

    DrawShape d;

    @Inject
    CircleRequest(@Circle DrawShape d) {
        this.d = d;
    }

    public void makeRequest() {
        d.draw();
    }
}
