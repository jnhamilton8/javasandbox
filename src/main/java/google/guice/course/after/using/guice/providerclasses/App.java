package google.guice.course.after.using.guice.providerclasses;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class App {

    private static final String SQUARE_REQ = "SQUARE";
    private static final String CIRCLE_REQ = "CIRCLE";

    public static void main(String[] args) {
        sendRequest(SQUARE_REQ);
        sendRequest(CIRCLE_REQ);
    }

    private static void sendRequest(String squareReq) {
        if (squareReq.equals(SQUARE_REQ)) {
            Injector injector = Guice.createInjector(new AppModule());
            //Guice sees in SquareRequest constructor it needs a DrawShape, and in the AppModule we
            //tell it to use DrawSquare as this is bound to DrawShape
            SquareRequest request = injector.getInstance(SquareRequest.class);
            request.makeRequest();
        }

        if (squareReq.equals(CIRCLE_REQ)) {
            Injector injector = Guice.createInjector(new AppModule());
            CircleRequest request = injector.getInstance(CircleRequest.class);
            request.makeRequest();
        }
    }
}
