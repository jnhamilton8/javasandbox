package google.guice.course.after.using.guice.providerclasses;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import google.guice.course.after.using.guice.annotations.*;

public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(DrawShape.class).annotatedWith(Square.class).toProvider(DrawSquareProvider.class).in(Scopes.SINGLETON); //create drawshape in singleton scope, so only one instance of squareRequest created in App
        bind(DrawShape.class).annotatedWith(Circle.class).toProvider(DrawCircleProvider.class).in(Scopes.SINGLETON);
        bind(String.class).annotatedWith(SquareColourValue.class).toInstance("Red");
        bind(Integer.class).annotatedWith(EdgeValue.class).toInstance(40);

        bind(String.class).annotatedWith(CircleColourValue.class).toInstance("Black");
        bind(Integer.class).annotatedWith(RadiusValue.class).toInstance(10);
    }
}
