package google.guice.course.after.using.guice.providerclasses;

public interface DrawShape {

    void draw();
}
