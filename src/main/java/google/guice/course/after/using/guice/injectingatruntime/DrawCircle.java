package google.guice.course.after.using.guice.injectingatruntime;

import static google.guice.course.after.using.guice.injectingatruntime.Constants.CIRCLE;

public class DrawCircle implements DrawShape {

    @Override
    public void draw() {
        System.out.println("Drawing Circle!");
    }

    @Override
    public String getShapeName() {
        return CIRCLE;
    }
}
