package google.guice.course.after.using.guice.injectingatruntime;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<DrawShape> drawShapeBinder = Multibinder.newSetBinder(binder(), DrawShape.class);
        drawShapeBinder.addBinding().to(DrawSquare.class);
        drawShapeBinder.addBinding().to(DrawCircle.class);
    }
}
