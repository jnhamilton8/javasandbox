package google.guice.course.after.using.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import google.guice.course.after.using.guice.annotations.*;

public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
       //   bind(DrawShape.class).annotatedWith(Square.class).to(DrawSquare.class);
       //  bind(DrawShape.class).annotatedWith(Names.named("Circle")).to(DrawCircle.class);
        bind(String.class).annotatedWith(SquareColourValue.class).toInstance("Red");
        bind(Integer.class).annotatedWith(EdgeValue.class).toInstance(40);
        bind(String.class).annotatedWith(CircleColourValue.class).toInstance("Blue");
        bind(Integer.class).annotatedWith(RadiusValue.class).toInstance(80);
    }


    @Provides
    @Singleton
    @Square
    DrawShape providesDrawSquare(@SquareColourValue String color, @EdgeValue Integer edge) {
        DrawShape d = new DrawSquare(color, edge);
        return d;
    }

    //tells Guice whenever it needs a Drawshape, use this method to obtain one
    @Provides
    @Singleton
    @Circle
    DrawShape providesDrawCircle(@CircleColourValue String color, @RadiusValue Integer radius) {
        DrawShape d = new DrawCircle(color, radius);
        return d;
    }
}
