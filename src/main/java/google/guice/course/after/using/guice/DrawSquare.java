package google.guice.course.after.using.guice;

public class DrawSquare implements DrawShape {

    private String color;
    private Integer edge;

    //If use Inject, you need to bind in AppModule as well
    /*@Inject
    public DrawSquare(@ColourValue String color, @EdgeValue Integer edge) {
        this.color = color;
        this.edge = edge;
    }*/

    public DrawSquare(String color, Integer edge) {
        this.color = color;
        this.edge = edge;
    }

    @Override
    public void draw() {
        System.out.println("Drawing Square of color : " + color + " and edge " + edge );
    }
}
