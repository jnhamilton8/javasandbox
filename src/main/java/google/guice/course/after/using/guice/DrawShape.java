package google.guice.course.after.using.guice;

public interface DrawShape {

    void draw();
}
