package google.guice.course.after.using.guice.providerclasses;

import com.google.inject.Inject;
import com.google.inject.Provider;
import google.guice.course.after.using.guice.annotations.CircleColourValue;
import google.guice.course.after.using.guice.annotations.RadiusValue;

public class DrawCircleProvider implements Provider<DrawCircle> {

    private String color;
    private Integer radius;

    @Inject
    public DrawCircleProvider(@CircleColourValue String color, @RadiusValue Integer radius) {
        this.color = color;
        this.radius = radius;
    }

    @Override
    public DrawCircle get() {
        return new DrawCircle(color, radius);
    }
}