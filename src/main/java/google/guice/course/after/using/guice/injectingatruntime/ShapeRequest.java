package google.guice.course.after.using.guice.injectingatruntime;

import com.google.inject.Inject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ShapeRequest {
   Map<String, DrawShape> shapeNameVSDrawShape;

    @Inject
    ShapeRequest(Set<DrawShape> set) {
        shapeNameVSDrawShape = new HashMap<>();

        for(DrawShape d: set) {
            shapeNameVSDrawShape.put(d.getShapeName(), d);
        }
    }

    public void makeRequest(String shapeName) {
       DrawShape d = shapeNameVSDrawShape.get(shapeName);
       d.draw();
    }
}