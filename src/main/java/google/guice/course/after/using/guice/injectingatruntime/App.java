package google.guice.course.after.using.guice.injectingatruntime;

import com.google.inject.Guice;
import com.google.inject.Injector;

import static google.guice.course.after.using.guice.injectingatruntime.Constants.SQUARE;

public class App {

    public static void main(String[] args) {
        sendRequest(SQUARE);
    }

    private static void sendRequest(String squareReq) {
        Injector injector = Guice.createInjector(new AppModule());
        ShapeRequest request = injector.getInstance(ShapeRequest.class);
        request.makeRequest(SQUARE);
    }
}