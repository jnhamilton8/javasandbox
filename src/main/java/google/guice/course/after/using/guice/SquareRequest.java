package google.guice.course.after.using.guice;
import com.google.inject.Inject;
import google.guice.course.after.using.guice.annotations.Square;

public class SquareRequest {
    //field injection
   // @Inject
    DrawShape d;

    //Constructor injection
   /* @Inject
    SquareRequest(@Square DrawShape d) {
        this.d = d;
    }*/

    @Inject
    SquareRequest(@Square DrawShape d) {
        this.d = d;
    }

    public void makeRequest() {
        d.draw();
    }

    //Method injection
    /*@Inject
    public void setDrawShape(DrawShape d) {
        this.d = d;
    }*/
}
