package google.guice.course.after.using.guice.injectingatruntime;

public interface DrawShape {
    void draw();
    String getShapeName();
}
