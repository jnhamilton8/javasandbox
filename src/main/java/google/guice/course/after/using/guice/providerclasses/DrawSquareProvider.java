package google.guice.course.after.using.guice.providerclasses;

import com.google.inject.Inject;
import com.google.inject.Provider;
import google.guice.course.after.using.guice.annotations.EdgeValue;
import google.guice.course.after.using.guice.annotations.SquareColourValue;

public class DrawSquareProvider implements Provider<DrawSquare> {

    private String color;
    private Integer edge;

    //Tells guice whenever it is producing an instance of DrawSquareProvider, use this constructor
    @Inject
    public DrawSquareProvider(@SquareColourValue String color, @EdgeValue Integer edge) {
        this.color = color;
        this.edge = edge;
    }

    @Override
    public DrawSquare get() {
        return new DrawSquare(color, edge);
    }
}