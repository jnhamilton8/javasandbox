package google.guice.course.after.using.guice.providerclasses;
import com.google.inject.Inject;
import google.guice.course.after.using.guice.annotations.Square;

public class SquareRequest {
    DrawShape d;

    @Inject
    SquareRequest(@Square DrawShape d) {
        this.d = d;
    }

    public void makeRequest() {
        d.draw();
    }

}
