package commonProblems;

import java.util.Arrays;

public class Strings {

    //Design an algorithm to determine if a string has all unique characters
    public boolean isUniqueChars(String str) {
        if (str.length() > 128) return false; //only 128 chars in ASCII alphabet, unless extended is used, then 255

        boolean[] charSet = new boolean[128];
        for (int i = 0; i < str.length(); i++) {
            int val = str.charAt(i); //implicitly converts the chat to it's int value
            if (charSet[val]) { //we have already found this char in string
                return false;
            }
            charSet[val] = true; //set this index of the array to true as we have now seen this char
        }
        return true;
    }

    //Design an algorithm that decides if one string is a permutation of the other e.g. acb --> bac
    String sort(String s) {
        char[] content = s.toCharArray(); //create array of the chars in the string
        Arrays.sort(content);  //then sort the chars and return a new sorted string
        return new String(content);
    }

    public boolean permutation(String s, String t) {
        if (s.length() != t.length()) return false; //if they are not the same length then one can't be a permutation of the other
        return sort(s).equals(sort(t)); //after sorting check the strings are the same
    }

    //Design an algorithm that replaces all spaces in a string with '%20', with space at the end to hold extra characters and are given the true length of the string

    public String replaceSpaces(char[] str, int trueLength) {
        int spaceCount = 0;
        for (int i = 0; i < trueLength; i++) {
            if (str[i] == ' ') {
                spaceCount++;
            }
        } //this section counts the number of spaces in the string

        //double the spaceCount to see how many extra chars we will need. As it's %20, and each space is one char, then need 2 extra chars
        //set index then to be the truelength plus the extra space needed when the spaces are replaced
        int index = trueLength + spaceCount * 2;


        //edit the string in reverse order.  Useful as we have a buffer at the end so we can change chars without worrying about overwriting.

        //character constant that will set the end of the array to the trueLength.
        //Algo assumes we will always have space at the end of the string to place the additional chars
        if (trueLength < str.length) str[trueLength] = '\0';

        for (int i = trueLength - 1; i >= 0; i--) { //work backwards through the string, remember 0 indexed so -1
            if (str[i] == ' ') { //if a space, then insert (overwrite) the next 3 slots (we have moved the chars to the end so it's fine)
                str[index - 1] = '0';
                str[index - 2] = '2';
                str[index - 3] = '%';
                index = index -3;
            } else {
                str[index - 1] = str[i]; //if no space, then we shift the char to the end
                index --;
            }
        }
        return new String(str);
    }

    /*Design an algorithm to check if a string is a permutation of a palindrome.
      So we need to know if the string can be written the same forward as backwards.  To be a palindrome, a string can have no more than one
      character that has an odd count.  We otherwise need an even number of characters e.g. civic (odd count for v).
      We will have to look through the entire string for this.
       */

    public boolean isPermutationOfPalindrome(String phrase) {
        int[] table = buildCharFrequencyTable(phrase);
        return checkMaxOneOdd(table);
    }

    /*Check that more more than one character has an odd count*/
    boolean checkMaxOneOdd(int[] table) {
        boolean foundOdd = false;
        for (int count : table) {
            if (count % 2 == 1) {
                if (foundOdd) {
                    return false;
                }
                foundOdd = true;
            }
        }
        return true;
    }

    /*Count how many times each character appears */
    int[] buildCharFrequencyTable(String phrase) {
        //getNumericValue gets unicode number, assigns value of 10-35 for a-z. This gives 25, and add 1 for 26 chars in alphabet
        //The table represents letters of the alphabet so index 0=a, 1=b, 2=c etc.
        int[] table = new int[Character.getNumericValue('z') - Character.getNumericValue('a') + 1]; //
        for (char c : phrase.toCharArray()) {
            int x = getCharNumber(c);
            if (x != -1) {
                table[x]++; //add 1 to that slot in the array i.e. increase the count
            }
        }
        return table;
    }

    /*Map each character to an index in the array. It is case insensitive.  a->0, b->1, c->2
     * Non letter characters map to -1*/
    int getCharNumber(Character c) {
        int a = Character.getNumericValue('a'); //10
        int z = Character.getNumericValue('z'); //35
        int val = Character.getNumericValue(c);
        if (a <= val && val <= z) { //check if val is a letter between a and z if not return -1
            return val - a; //a is beginning of hashmap i.e at index 0.  So get index of our 26 slot hash by letter-a (which is 10)
        }
        return -1;
    }

    //Design an algorithm that, given two strings, checks if they are one edit away (in terms of insertion, removal, replace)
    //examples: pale, ple, pales, pale, pale, bale --> true.  lae --> false
    //Think what these mean.  Insertion/removal are both shifts at some point in the strings and are identical, one is just the inverse of the other.  e.g. apple, aple
    //Insert on second, remove on first
    //Replacement they are only different in one place.

    public boolean oneEditAway(String first, String second) {
        if (first.length() == second.length()) { //if the are of the same length then it check if they are one edit on replace
            return oneEditReplace(first, second);
        } else if (first.length() + 1 == second.length()) { //if the first is shorter than second by one character (otherwise not one edit) then check insertion on first
            return oneEditInsert(first, second);
        } else if (first.length() - 1 == second.length()) { //if the first is longer than second by one character (otherwise not one edit) then check insertion on second
            return oneEditInsert(second, first);
        }
        return false; //if the strings are > 1 character apart in length immediately return false
    }

    //check if you can insert a character into S1 to make S2
    private boolean oneEditInsert(String first, String second) {
        int index1 = 0; //pointer1
        int index2 = 0; //pointer2
        while (index2 < second.length() && index1 < first.length()) { //while the pointers are < length of each strings, second is the shortest
            if (first.charAt(index1) != second.charAt(index2)) { //if the two chars at the current pointers are not the same
                if (index1 != index2) { //if the pointers are at different places in the respective strings
                    return false; //this means we have found different chars AND the pointers are not in same place.  Means > 1 difference and not one edit.
                }
                index2++; //the first string passed in is always the shortest, we have found chars in both that do not match here, so increment second pointer and carry on
            } else { //if the chars in the strings are the same, simply move both pointers along
                index1++;
                index2++;
            }
        }
        return true;
    }

    private boolean oneEditReplace(String first, String second) {
        boolean foundDifference = false;
        for (int i = 0; i < first.length(); i++) { //iterate to length of first string which will be the shortest
            if (first.charAt(i) != second.charAt(i)) {  //if chars are not equal
                if (foundDifference) { //false initially so falls through, if hit again, then they are different by > 1 char = multiple replaces, so set to false
                    return false;
                }
                foundDifference = true; // chars are not the same, so have found a difference
            }
        }
        return true; //for the case where the strings are the same, won't pass the if condition
    }

    //Design an algorithm to perform basic string compression using the counts of repeated characters.
    //Example string aabcccccaaa would become a2b1c5a3
    //Return the original string if the compressed string does not become smaller than the original string. Has a-z chars only
    //Notes - it involves string concatenation, so use Stringbuilder.  String concatenation operates in O(n^2) time.

    public String compress(String str) {
        StringBuilder compressed = new StringBuilder();  //initialise StringBuilder object
        int countConsecutive = 0;
        for (int i = 0; i < str.length(); i++) {
            countConsecutive++; //increment count by one initially, and by one each time the chars next to each other are the same

            //If next character is different than current, append this char and count to result
            if (i + 1 >= str.length() || str.charAt(i) != str.charAt(i + 1)) { //use guard if i+1 >= str.length() as will have reached end of string, so avoid indexOutOfBoundsException
                compressed.append(str.charAt(i)); //append the char as it's different
                compressed.append(countConsecutive); //then append the current count of that char
                countConsecutive = 0; //reset count to 0 ready for the next consecutive chars (if any)
            }
        }
        return compressed.length() < str.length() ? compressed.toString() : str; //if  the compressed string is NOT smaller, then return original string, else return compressed string
    }

    //Design an algorithm that given an image represented y a NxN matrix, rotates the image by 90 degrees.
    //Algo is O(N^2) as it must touch all N^2 elements of the matrix
    //Need to rotate it layer by layer, layer 1 the outside, layer 2 next in, layer 3 etc.  All by 90 degrees.
    //Breaking it down, need to rotate each cell of each layer by 90 degrees.
    //Start with left most cell of layer 1 and move to end of row 1. etc.

    public int[][] rotate(int[][] matrix) {
        //to rotate, must be a square with same number of rows and columns
        if (matrix.length == 0 || matrix.length != matrix[0].length) throw new IllegalArgumentException("Matrix must be a square");

        int length = matrix.length-1; //length of each row, -1 as zero indexed e.g. 4

        for (int i = 0; i <= length/2; i++) { //iterate for no of layers to rotate in square, which is length / 2
            for (int j = i; j < length-i; j++) {
                //inner loop starts from where outer loop starts
                //length-i because in each iteration, you are rotating a layer. So say matrix is 5*5, in first iteration outer layer
                //is length 4 (4-i= 4-0). The outer layer is then rotated.
                //i is then 1, as onto layer 2.  So no need to rotate the outer layer it's done.
                //Layer 2 is length 4-i = 4-1 = 3. And you start at index 1.  So go from index 1 to which is the second layer!

                //Collect data of all 4 cells that need to be rotated before swapping
                //Store data at top left of top row of layer [0][0] (then second layer would start from [1][1])
                int p1 = matrix[i][j];

                //Store data at end right of top row of layer [0][4]
                int p2 = matrix[j][length-i];

                //Store data at end right of bottom row of layer [4][4]
                int p3 = matrix[length-i][length-j];

                //Store data at first left of bottom row of layer [4][0]
                int p4 = matrix[length-j][i];

                //Swap values of 4 values stored above
                matrix[j][length-i] = p1;  //Move top left [0][0] to [0][4] end top right
                matrix[length-i][length-j] = p2; //Move end top right [0][4] to [4][4] bottom right
                matrix[length-j][i] = p3; //Move bottom right [4][4] to [4][0] bottom left
                matrix[i][j] = p4; //Move bottom left [4][0] to [0][0] top right
            }
        }
        return matrix;
    }

    //Design an Algorithm that takes a M*N matrix, and if an element in that matrix is 0 the entire row and column are set to 0
    //The idea with this is that we will track all of rows with 0s and all the columns with 0s. Then nullify rows/columns based on the values in these arrays
    //Complexity is O(N^2) and space is O(N) where N is the size of the matrix

    public int[][] setZeros(int[][] matrix) {
        boolean[] row = new boolean[matrix.length]; //will keep track of the rows with zeros
        boolean[] column = new boolean[matrix[0].length]; //will keep track of the columns with zero

        //Store the row and columns index with value 0
        for (int i = 0; i < matrix.length; i++) { //iterate over row
            for (int j = 0; j < matrix[0].length; j++) { //iterate over each column in each row
                if (matrix[i][j] == 0) { //if the current cell has value of 0
                    row[i] = true; //set that index in rows array to true
                    column[j] = true; //set that index in columns array to true
                }
            }
        }

        //nullify the rows
        for (int i = 0; i < row.length; i++) { //iterate over the rows array
            if (row[i]) nullifyRow(matrix, i); //if the value in the rows array at index i is true, go ahead and nullify the row
        }

        //nullify the columns
        for (int j =0; j < column.length; j++) {
            if (column[j]) nullifyColumn(matrix, j);

        }

        return matrix;
    }

    private void nullifyColumn(int[][] matrix, int column) {
        for (int i = 0; i < matrix.length; i++) { //iterate over the number of rows in the matrix
            matrix[i][column] = 0; //goes down each row in the matrix [i], and for that column [column] sets the values to 0
        }
    }

    private void nullifyRow(int[][] matrix, int row) {
        for (int j = 0; j < matrix[0].length; j++) { //iterate over the number of columns in the matrix (remember it is a square)
            matrix[row][j] = 0; //goes across the row in the matrix [row], and for each column in that row [j] sets the values to 0
        }
    }

    //Design an algorithm that given 2 strings, checks if S2 is a rotation of S1, only having one call to a method isSubString
    //e.g. erbottlewat is rotation of waterbottle
    //Think about what the rotation point is.  In a rotation, S1 is cut into two parts, x and y, and we rearrange them to get S2
    //s1 = xy = waterbottle.  x = wat.  y = erbottle.  S2 = yx = erbottlewat!
    //So, we can see that yx will always be a subsstring of xyxy e.g. waterbottlewaterbottle. So S2 will always be a substring of S1S1.
    //Runtime is O(N) of the algo if we assume contains() runs in O(A+B) time, length of two strings which we know is even here in worst case
    //as we test for it

    public boolean isRotation(String s1, String s2) {
        //Check both strings are of equal length and not empty
        if (s1.length() == s2.length() && s1.length() > 0) {
            //create s1s1 ready to test if s2 is substring
            String s1s1 = s1 + s1;
            return isSubString(s1s1, s2);
        }
        return false;
    }

    private boolean isSubString(String s1s1, String s2) {
        return s1s1.toLowerCase().contains(s2.toLowerCase());
    }
}