package commonProblems;

/*
The key to thinking recursively is to see the solution to the problem as a smaller version of the same problem.
The key to solving recursive programming requirements is to imagine that your method does what its name says it does even
before you have actually finished writing it. You must pretend the method does its job and then use it to solve the more complex cases. Here is how.

Identify the base case(s) and what the base case(s) do. A base case is the simplest possible problem (or case) your method could be passed.
Return the correct value for the base case. Your recursive method will then be comprised of an if-else statement where the base case returns
one value and the non-base case(s) recursively call(s) the same method with a smaller parameter or set of data.
Thus you decompose your problem into two parts: (1) The simplest possible case which you can answer (and return for), and (2) all other more
complex cases which you will solve by returning the result of a second calling of your method. This second calling of your method ( recursion )
will pass on the complex problem but reduced by one increment. This decomposition of the problem will actually be a complete,
accurate solution for the problem for all cases other than the base case.
Thus, the code of the method actually has the solution on the first recursion.
*/

public class Recursion {

    public void simpleRecursiveMethod(int counter) {
        if (counter == 0) return;
        else {
            System.out.println("Counter is: " + counter);
            //hits recursive method and places 4 frames on the stack as it is called 4 times, and prints 4, 3, 2, 1
            simpleRecursiveMethod(--counter);
            //when it is 0 it returns here - prints out 0, then goes through each of the simpleRecursiveMethod frames of
            //which there are 4, starting with the one at 0, and print 0, 1, 2, 3
            System.out.println("Counter is again: " + counter);
        }
    }

    //!4 = 4*3*2*1. Equation:  n! = 1 if n = 0, or n * (n-1)! e.g. n*(factorial(n-1))
    public int factorialRecursion(int n) {
        if (n == 1) return 1;
        else {
            return (n * (factorialRecursion(n - 1)));
        }
    }

    static void main(String[] args) {
        String str = "Test";
        String reversed = reverseString(str);
        System.out.println("The reversed string is: " + reversed);
    }

    private static String reverseString(String str) {
        if (str.isEmpty())
            return str;
        //Calling Function Recursively
        return reverseString(str.substring(1)) + str.charAt(0);
    }
}